package org.nebula.was_devops;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParsedId {

	public static Pattern ID_PATTERN = Pattern.compile("\"?([^(]+)?\\(([^)|]+)\\|([^)#]+)#([^_]+)_([^)]+)\\)\"?");

	private final String id;

	private final Matcher matcher;

	private final Map<String, String> pathMap = new HashMap<String, String>();

	public ParsedId(String id) {
		super();
		this.id = id;
		this.matcher = ID_PATTERN.matcher(id);
		if (!matcher.matches()) {
			throw new IllegalArgumentException(id + " is not a valid WebSphere AdminConfig ID structure");
		}
		String path = getPath();
		String[] tokens = path.split("/");
		for (int i = 0; i < tokens.length; i = i + 2) {
			if (tokens.length >= i + 2) {
				pathMap.put(tokens[i], tokens[i + 1]);
			}
		}
	}

	public String getScopeContainment() {
		StringBuilder sb = new StringBuilder("/");
		String cell = getCell();
		String node = getNode();
		String server = getServer();
		String cluster = getCluster();
		String application = getApplication();

		if (cell != null) {
			sb.append("Cell:").append(cell).append("/");
		}
		if (node != null) {
			sb.append("Node:").append(node).append("/");
		}
		if (server != null) {
			sb.append("Server:").append(server).append("/");
		}
		if (cluster != null) {
			sb.append("Cluster:").append(cluster).append("/");
		}
		if (application!= null) {
			sb.append("Application:").append(application).append("/");
		}
		return sb.toString();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return matcher.group(1);
	}

	public String getPath() {
		return matcher.group(2);
	}

	public Map<String, String> getPathMap() {
		return pathMap;
	}

	public String getFile() {
		return matcher.group(3);
	}

	public String getType() {
		return matcher.group(4);
	}

	public String getXMI() {
		return matcher.group(5);
	}

	public String getCell() {
		return pathMap.get("cells");
	}

	public String getNode() {
		return pathMap.get("nodes");
	}

	public String getCluster() {
		return pathMap.get("clusters");
	}

	public String getApplication() {
		return pathMap.get("applications");
	}

	public String getServer() {
		return pathMap.get("servers");
	}

	public String getBus() {
		return pathMap.get("buses");
	}
}