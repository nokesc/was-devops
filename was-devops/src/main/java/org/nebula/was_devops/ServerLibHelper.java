package org.nebula.was_devops;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerLibHelper {
	public static String PATH_SEP = System.getProperty("path.separator");

	public static final String SERVER_LIB_DIR = System.getProperty("was-devops.server-lib.dir");

	private static File getDefaultLocation() {
		if (SERVER_LIB_DIR != null && SERVER_LIB_DIR.length() > 0) {
			return new File(SERVER_LIB_DIR);
		}
		return getCanonicalFile(new File(System.getProperty("user.home"), ".was-devops/server-lib"),
				".was-devops/server-lib");
	}

	private static File getCanonicalFile(File file, String path) {
		try {
			return file.getCanonicalFile();
		} catch (IOException e) {
			throw new IllegalArgumentException(path, e);
		}
	}

	private File serverLibDir;

	public ServerLibHelper() {
		this(getDefaultLocation());
	}
	
	public ServerLibHelper(String serverLibDir) {
		this(new File(serverLibDir));
	}

	public ServerLibHelper(File serverLibDir) {
		super();
		this.serverLibDir = serverLibDir;
	}

	public File getServerLibDir() {
		return serverLibDir;
	}

	public void setServerLibDir(File serverLibDir) {
		this.serverLibDir = serverLibDir;
	}

	public String serverLibPath(String subPath) {
		return serverLibLoc(subPath).getPath();
	}

	public File serverLibLoc(String subPath) {
		return getCanonicalFile(new File(serverLibDir, subPath), subPath);
	}

	public String jarPaths(String subPath) {
		StringBuilder result = new StringBuilder(32);
		for (File jarLoc : jarLocs(subPath)) {
			if (result.length() > 0) {
				result.append(PATH_SEP);
			}
			result.append(jarLoc.getPath());
		}
		System.out.println(result);
		return result.toString();
	}

	public List<File> jarLocs(String subPath) {
		List<File> result = new ArrayList<File>();
		File dirOfJars = serverLibLoc(subPath);
		if (dirOfJars.isDirectory()) {
			for (File child : dirOfJars.listFiles()) {
				if (child.getName().toLowerCase().endsWith(".jar")) {
					result.add(child);
				}
			}
		}
		return result;
	}
}