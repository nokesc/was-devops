package org.nebula.was_devops;

public class UnbalancedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnbalancedException(String str, int index) {
		super(String.format("index=%s %s", index, str));
	}
}