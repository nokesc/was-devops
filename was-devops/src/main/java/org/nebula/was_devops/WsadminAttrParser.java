package org.nebula.was_devops;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class WsadminAttrParser {

	private static class OpenBracket {
		char c;
		int location;

		public OpenBracket(char c, int location) {
			super();
			this.c = c;
			this.location = location;
		}
	}

	private static final Map<Character, Character> DEFAULT_closeToOpen = new HashMap<Character, Character>();

	static {
		DEFAULT_closeToOpen.put(']', '[');
	}

	private final Map<Character, Character> closeToOpen;

	private final Set<Character> open;

	private String input;
	private String lastComplexValue;
	private Deque<OpenBracket> openBracketStack;
	HashMap<String, String> map;
	OpenBracket lastPop;
	private int i;
	private char c;

	public WsadminAttrParser() {
		this(DEFAULT_closeToOpen);
	}

	public WsadminAttrParser(Map<Character, Character> closeToOpen) {
		super();
		this.closeToOpen = closeToOpen;
		this.open = new HashSet<Character>(closeToOpen.values());
	}

	public Map<String, String> parse(String input) {
		this.input = input.trim();
		openBracketStack = new ArrayDeque<OpenBracket>();
		i = 0;
		map = new HashMap<String, String>();
		// System.out.println("***********************************");
		process();
		// System.out.println("===================================\n\n");
		return map;

	}

	private void process() {
		for (int size = input.length(); i < size; i++) {
			c = input.charAt(i);
			Character expectedOpenForCurrentClose = closeToOpen.get(c);
			if (expectedOpenForCurrentClose == null && !open.contains(c)) {
				handleChar();
			} else {
				if (expectedOpenForCurrentClose != null) {
					doPop(expectedOpenForCurrentClose);
					handleClose();
				} else {
					openBracketStack.push(new OpenBracket(input.charAt(i), i));
					handleOpen();
				}
			}
		}
		if (!openBracketStack.isEmpty()) {
			throw new UnbalancedException(input, i);
		}
	}

	private void doPop(char expectedOpenForCurrentClose) {
		if (openBracketStack.isEmpty()) {
			throw new UnbalancedException(input, i);
		}
		lastPop = openBracketStack.pop();
		if (lastPop.c != expectedOpenForCurrentClose) {
			throw new UnbalancedException(input, i);
		}
	}

	private void handleOpen() {

	}

	private void handleChar() {
		// TODO Auto-generated method stub

	}

	private void handleClose() {
		// System.out.println("**");
		String token = input.substring(lastPop.location + 1, i);
		// System.out.println("token={" + token + "}");
		int depth = openBracketStack.size();
		// System.out.println(depth);
		if (depth == 1) {
			String[] kv = token.split(" ");
			String value = kv[1];
			if (lastComplexValue != null) {
				value = lastComplexValue;
				lastComplexValue = null;
			}
			// System.out.format("[%s]=[%s]\n", kv[0], value);
			map.put(kv[0], value);
		}
		if (depth == 2) {
			lastComplexValue = token;
			// System.out.format("[%s]\n", lastComplexValue);
		}

		// System.out.println("***");
	}
}