import utils
import unittest

class TestUtils(unittest.TestCase):
	def testToSet(self):
		hashSet = utils.toSet("a", "b", "c")
		self.assertEquals(1, hashSet.contains("a"))
		self.assertEquals(1, hashSet.contains("b"))
		self.assertEquals(1, hashSet.contains("c"))
		self.assertEquals(3, hashSet.size())

	def testId(self):
		idHelper = utils.IdHelper("t3(cells/Chris-THINKNode01Cell/buses/t3|sib-bus.xml#SIBus_1446947100004)")
		self.assertEquals("Chris-THINKNode01Cell", idHelper.getCell())
		print str(idHelper)