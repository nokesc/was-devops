import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server

class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()

	def tearDown(self):
		AdminConfig.reset()

	def validateResolve(self, parent, data):
		if parent:
			print "validateResolve: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
	def _JDBCProvider(self):
		self.validateResolve(server, {
			"_type": "JDBCProvider",
			"_impl": "OracleXAJDBCProvider",
			"name": "Oracle JDBC Driver (XA)",
			"_init": {
				"scope": server.getIdHelper().getJDBCProviderAdminTask()
			}
		})

	def test_cOracleXAJDBCProvider(self):
		jdbcProvider = server.OracleXAJDBCProvider({})
		jdbcProvider.Oracle11gDS({
			"jndiName": "jdbc/ods1",
			"URL": "myurl",
			"_customProps": {
				"connectionProperties": "oracle.jdbc.createDescriptorUseCurrentSchemaForSchemaName=true"
			}
		})
		
	def test_cOracleJDBCProvider(self):
		jdbcProvider = server.OracleJDBCProvider({})
		jdbcProvider.Oracle11gDS({
			"jndiName": "jdbc/ods1",
			"URL": "myurl",
			"_customProps": {
				"connectionProperties": "oracle.jdbc.createDescriptorUseCurrentSchemaForSchemaName=true"
			}
		})