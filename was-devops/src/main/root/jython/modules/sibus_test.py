import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server
sibAdapter = server.getSIBJMSResourceAdapter()

sibusName = "UT_BUS"
sibus = {
	"_type": "SIBus",
	"name": sibusName
}

sibusMember = {
	"_type": "SIBusMember",
	"_init": {
		"bus": sibusName
	},
	"server": server.getName(),
	"node": node.getName()
}

sibQueue = {
	"_type": "SIBQueue",
	"_init": {
		"bus": sibusName,
		"server": server.getName(),
		"node": node.getName()
	},
	"identifier": "q1",
}

sibQCF = {
	"_type": "J2CConnectionFactory",
	 "_impl": "SIBJMSQCF",
	 "_init": {
	 	"busName": sibusName
	 },
	"name": "sibQCF1",
	"jndiName": "jms/sibQCF1"
}

sibJMSQueue = {
	"_type": "J2CAdminObject",
	 "_impl": "SIBJMSQueue",
	 "_init": {
	 	"busName": sibusName
	 },
	"name": "sibQ1",
	"jndiName": "jms/sibQ1",
	"QueueName": sibQueue["identifier"]
}

sibActivationSpec = {
	"_type": "J2CActivationSpec",
	 "_impl": "SIBJMSActivationSpec",
	 "_init": {
	 	"busName": sibusName
	 },
	"name": "sibA1",
	"jndiName": "jms/sibA1",
	"destinationJndiName": sibJMSQueue["jndiName"]
}

AdminConfig.save()

class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()
		cell.remove(sibus)
		AdminConfig.save()	
		
	def tearDown(self):
		AdminConfig.reset()	

	def validateCreation(self, parent, data):
		if parent:
			print "validateCreation: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
	def test_SIBus(self):
		self.validateCreation(cell, sibus)
		
	def test_SIBusMember(self):
		sibusObj = config.resolveSIBus(sibus)
		self.validateCreation(sibusObj, sibusMember)

	def test_SIBQueue(self):
		sibusObj = config.resolveSIBus(sibus)
		sibusMemberObj = sibusObj.resolve(sibusMember)
		self.validateCreation(sibusObj, sibQueue)
		
	def test_SIBJMSQCF(self):
		sibusObj = config.resolveSIBus(sibus)
		self.validateCreation(sibAdapter, sibQCF)

	def test_SIBJMSQueue(self):
		sibusObj = config.resolveSIBus(sibus)
		sibusMemberObj = sibusObj.SIBusMember(sibusMember)
		self.validateCreation(sibAdapter, sibJMSQueue)
		
	def test_SIBActivationSpec(self):
		sibusObj = config.resolveSIBus(sibus)
		sibusMemberObj = sibusObj.SIBusMember(sibusMember)
		sibJMSQueueObj = sibAdapter.SIBJMSQueue(sibJMSQueue)
		self.validateCreation(sibAdapter, sibActivationSpec)
		