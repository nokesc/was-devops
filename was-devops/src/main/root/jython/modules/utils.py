import copy

import java.io.File as File
import java.io.FileInputStream as FileInputStream

import java.lang.String as String
import java.lang.StringBuilder as SB
import java.lang.System as System

import java.util.regex.Pattern as Pattern

import java.util.LinkedHashSet as HashSet
import java.util.LinkedHashMap as LinkedHashMap
import java.util.Properties as Properties

import org.nebula.was_devops.ParsedId as ParsedId
import org.nebula.was_devops.ServerLibHelper as ServerLibHelper
import org.nebula.was_devops.WsadminAttrParser as WsadminAttrParser

debugOn = 0

def debug(val):
	if debugOn:
		print str(val)

class IdHelper:
	
	
	def __init__(self, id):
		self.id = id
		self.parsedId = ParsedId(id)

	def getScopeId(self):
		path = self.getScopeContainment()
		return AdminConfig.getid(path)

	def getJDBCProviderAdminTask(self):
		cell = self.getCell()
		node = self.getNode()
		server = self.getServer()
		cluster = self.getCluster()
		application = self.getApplication()

		if server:
			return "Node=%s,Server=%s" % (node, server)

		if node:
			return "Node=%s" % (node)

		if cell:
			return "Cell=%s" % (cell)

		if cluster:
			return "Cluster=%s" % (cluster)
		
		if application:
			return "Application=%s" % (application)

	def getScopeContainment(self):
		return self.parsedId.getScopeContainment()

	def getId(self):
		return self.id

	def getName(self):
		return self.parsedId.getName()

	def getPath(self):
		return self.parsedId.getPath()

	def getFile(self):
		return self.parsedId.getFile()

	def getType(self):
		return self.parsedId.getType()

	def getXMI(self):
		return self.parsedId.getXMI()

	def getCell(self):
		return self.parsedId.getCell()

	def getNode(self):
		return self.parsedId.getNode()

	def getCluster(self):
		return self.parsedId.getCluster()

	def getApplication(self):
		return self.parsedId.getApplication()

	def getServer(self):
		return self.parsedId.getServer()

	def getBus(self):
		return self.parsedId.getBus()

def toSet(*vals):
	'''
	>>> toSet("a", "b", "c")
	[a, b, c]
	'''
	result = HashSet()
	for val in vals:
		result.add(val)
	return result

def mapToNestedList(dictionary):
	result = []
	for entry in dictionary.entrySet():
		key = entry.getKey()
		value = entry.getValue()
		if value.__class__.__name__ == "java.util.LinkedHashMap":
			value = mapToNestedList(value)
		result.append([key, value])
	return result


def mapToDictionary(m):
	result = {}
	for entry in m.entrySet():
		result[entry.getKey()] = entry.getValue()
	return result
		
def parseAdminTaskAttrs(val):
	parser = WsadminAttrParser()
	hashMap = parser.parse(val)
	return mapToDictionary(hashMap)

def dictionaryToNestedList(dictionary):
	'''
	>>> dictionaryToNestedList({"a": "b", "c": {"d": "e"} })
	[['a', 'b'], ['c', [['d', 'e']]]]
	'''
	result = []
	for key, value in dictionary.items():
		if value.__class__.__name__ == "org.python.core.PyDictionary":
			value = dictionaryToNestedList(value)
		result.append([key, value])
	return result
	
def forAdminTask(pythonDictionary):
	return dictionaryToAdminTaskData(pythonDictionary, prefix="", suffix="")
	
def forAdminConfig(pythonDictionary):
	return dictionaryToNestedList(pythonDictionary)

def dictionaryToAdminTaskData(dictionary, prefix="[", suffix="] "):
	result = SB()
	result.append(prefix)
	for key, value in dictionary.items():
		result.append("-").append(key).append(" ")
		if value.__class__.__name__ == "org.python.core.PyDictionary":
			value = dictionaryToAdminTaskData(value)
			result.append("[").append(value).append("] ")
		elif value.__class__.__name__ == "org.python.core.PyList":
			result.append(adminTaskList(value))
		else:
			result.append('"').append(value).append('" ')
	result.append(suffix)
	return result.toString()

def adminTaskList(value):
	result = SB()
	result.append("[")
	count = 0
	for child in value:
		if count > 0:
			result.append(" ")
		count = count + 1
		if child.__class__.__name__ == "org.python.core.PyDictionary":
			childVal = dictionaryToAdminTaskData(child)
			result.append(childVal)
		elif child.__class__.__name__ == "org.python.core.PyList":
			childVal = adminTaskList(child)
			result.append(childVal)
		else:
			result.append('"').append(child).append('" ')
	result.append("] ")
	return result.toString()

def attrBlobToMap(attrBlob):
	result = {}
	# TODO: Handle multi-line values and values with brackets
	for line in attrBlob.splitlines():
		line = line[1:len(line) - 1]
		spacePos = line.find(' ')
		key = line[0:spacePos]
		value = line[spacePos + 1:]
		result[key] = value
	return result
	
def getValue(dictionary, key):
	if key not in dictionary.keySet:
		return ""
	return dictionary[key]

def childDict(dictionary, key):
	if not dictionary.get(key):
		dictionary[key] = {}
	return dictionary[key]

def isList(val):
	if not val:
		return 0
	return val.__class__.__name__ == "org.python.core.PyList"

def merge(data, mergeData):
	if data and mergeData:
		for key, value in mergeData.items():
			if not data.get(key):
				data[key] = value
		return data

# ServerLib utils
slh = ServerLibHelper()
print "slh.getServerLibDir()=" + str(slh.getServerLibDir())

def jarPaths(root):
	print "root: " + root
	result = slh.jarPaths(root)
	print str(result)
	return result
	
def serverLibPath(name):
	return slh.serverLibPath(name)

def jarLocs(path):
	return slf.jarLocs(path)
	
def serverLibProps(path):
	envFile = serverLibPath(path)
	f = File(envFile)
	return loadPropsFromFile(f)
	
def loadPropsFromFile(f):
	envProps = Properties()
	fis = FileInputStream(f)
	envProps.load(fis)
	fis.close()
	return mapToDictionary(envProps)	

def loadProps(path):
	f = File(path).getCanonicalFile()
	return loadPropsFromFile(f)
	
def loadEnv():
	result = System.getProperty("profile.env")
	if("unknown" != result):
		print "Setting env from System property 'profile.env'"
		return result
	
	f = File("env.properties").getCanonicalFile()
	print "Attempting to load env from " + str(f)
	if f.exists():
		try:
			props = loadPropsFromFile(f)
			print str(props)
			return props["env"]
		except:
			print "Unable to load env from " + str(f)
	return None

env = loadEnv()
print "utils.env=" + str(env)