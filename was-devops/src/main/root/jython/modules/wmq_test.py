import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server
mqProvider = server.getWebSphereMQJMSProvider()
mqRA = server.getWebSphereMQResourceAdapter()

qcf1 = {
	"_type": "MQQueueConnectionFactory",
	"name": "jms.qcf2",
	"jndiName": "jms/qcf2",
	"host": "localhost",
	"port": "1234",
	"queueManager": "QMGR1",
	"XAEnabled": "true"	
}

q1 = {
	"_type": "MQQueue",
	"name": "jms.testq1",
	"description": "Description for testq1",
	"jndiName": "jms/testq1",
	"baseQueueName": "testq1"
}

as1 = {
	"_type": "J2CActivationSpec",
	"_impl": "WMQActivationSpec",
	"name": "jms.wsmqAS1",
	"jndiName": "jms/wmqAS1",
	"destinationJndiName": q1["jndiName"],
	"hostName": "localhost",
	"port": "1234",
	"queueManager": "QMGR1"
}

class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()
		mqProvider.remove(qcf1)
		mqProvider.remove(q1)
		AdminConfig.save()
		
	def tearDown(self):
		AdminConfig.reset()		

	def validateCreation(self, parent, data):
		
		print "validateCreation: " + str(parent.getId())
			
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
	def test_MQQueueConnectionFactory(self):
		self.validateCreation(mqProvider, qcf1)
			
	def test_MQQueue(self):
		self.validateCreation(mqProvider, q1)
		
	def test_WMQActivationSpec(self):
		self.validateCreation(mqRA, as1)
	
