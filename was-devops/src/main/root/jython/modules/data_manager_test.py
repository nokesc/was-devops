import config
import data_manager as dm
import unittest

dm1 = dm.DataManager({
	"_type": "MQQueueConnectionFactory",
	"XAEnabled": "true",
	"host": "localhost",
	"jndiName": "jms/qcf2",
	"name": "jms.qcf2",
	"port": "1234",
	"queueManager": "QMGR1",
	"rcvExit": "abc"
})

dm1.show()

cell1 = dm.DataManager({
	"_type": "Cell"
})

class TestDataManager(unittest.TestCase):
	def test_identityName(self):
		data = dm1.createDataForAdminTask()
		self.assertEquals("jms/qcf2", data.get("jndiName"))
		
		data = dm1.createDataForAdminConfig()
		self.assertEquals("jms/qcf2", data.get("jndiName"))
		self.assertEquals(0, dm1.isRootObject())
		
	def test_mappedAdminTaskName(self):		
		data = dm1.createDataForAdminTask()
		self.assertEquals(None, data.get("qgmrName"))
		self.assertEquals(None, data.get("queueManager"))
		
		data = dm1.createDataForAdminConfig()
		self.assertEquals(None, data.get("qgmrName"))
		self.assertEquals("QMGR1", data.get("queueManager"))
		
	def test_undefinedAdminTaskAttribute(self):		
		data = dm1.createDataForAdminTask()
		self.assertEquals(None, data.get("rcvExit"))
		
		data = dm1.createDataForAdminConfig()
		self.assertEquals("abc", data.get("rcvExit"))
		
		
	def test_undefinedAdminTaskAttribute(self):		
		data = dm1.createDataForAdminTask()
		self.assertEquals(None, data.get("rcvExit"))
		
		data = dm1.createDataForAdminConfig()
		self.assertEquals("abc", data.get("rcvExit"))
		
	def test_rootObject(self):
		self.assertEquals(1, cell1.isRootObject())
		
	def test_implTaskInit(self):
		h1 = dm.getTypeHelper("JDBCProvider", None)
		self.assertEquals(None, h1.taskInit)
		i1 = h1.getImpl("OracleXAJDBCProvider")
		self.assertNotEquals(None, i1)
		
		self.assertNotEquals(None, i1.taskInit)
		self.assertEquals("Oracle", i1.taskInit["databaseType"])
	
	def test_customInitHandler(self):
		h1 = dm.getTypeHelper("JDBCProvider", None)
		self.assertNotEquals(None, h1.customInitHandler)
		i1 = h1.getImpl("OracleXAJDBCProvider")	
		self.assertNotEquals(None, i1)
		
		self.assertEquals(h1.customInitHandler, i1.customInitHandler)
		
	def test_processor(self):
		h1 = dm.getTypeHelper("JDBCProvider", None)
		self.assertNotEquals(None, h1.processor)
		i1 = h1.getImpl("OracleXAJDBCProvider")	
		self.assertNotEquals(None, i1)
		
		self.assertEquals(h1.processor, i1.processor)
		
	def test_configToTaskMapping(self):
		h1 = dm.getTypeHelper("J2CActivationSpec", None)
		self.assertEquals(None, h1.processor)
		self.assertNotEquals(None, h1.configToTaskMapping)
		i1 = h1.getImpl("WMQActivationSpec")	
		self.assertNotEquals(None, i1)
		
		self.assertEquals(h1.configToTaskMapping, i1.configToTaskMapping)
		self.assertNotEquals(None, i1.processor)
		
	def test_customPropMapping(self):
		h1 = dm.getTypeHelper("J2CActivationSpec", None)
		self.assertEquals(None, h1.customPropMapping)
		i1 = h1.getImpl("WMQActivationSpec")	
		self.assertNotEquals(None, i1.customPropMapping)
		
		self.assertNotEquals(h1.customPropMapping, i1.customPropMapping)