import copy
import utils
import config
import config_meta
import java.lang.String as String
import traceback

# Move to data_manager
def drv(data):
	name = data.get("name")
	jndiName = data.get("jndiName")
	if not name and jndiName:
		data["name"] = String(jndiName).replace("/", ".")
	if name and not jndiName:
		data["jndiName"] = String(name).replace(".", "/")
	return data

################################################################################
## Adapter for invoking AdminTask operation using normalized DataManager data
class AT:
	# adminTaskOp:  The AdminTask operation to invoke
	# useParentIdArg: Sometimes the parentId is needed as the first AdminTask argument.  Othertimes, AdminTask derives the parentId from the data required by the task.
	# useParentScopeForFirstArg: A few operations require the scope of the parent id be used for the parent id
	def __init__(self, adminTaskOp, useParentIdArg = 1, useParentScopeForFirstArg = 0):
		self.adminTaskOp = adminTaskOp
		self.useParentIdArg = useParentIdArg
		self.useParentScopeForFirstArg = useParentScopeForFirstArg
	
	def adminTask(self, parentId, dm):
		# Calculate the parentArg to use
		parentArg = parentId
		if self.useParentScopeForFirstArg:
			idHelper = utils.IdHelper(parentId)
			parentArg = idHelper.getScopeId()

		# Calculate the data
		configData = dm.createDataForAdminTask()

		customInitHandler = dm.getTypeHelper().customInitHandler
		if customInitHandler:
			customInitHandler.handle(parentArg, dm, configData)

		configData = utils.forAdminTask(configData)

		# Prepare response variable
		childId = None
		
		# Call AdminTask depending on number of args
		if self.useParentIdArg:
			print ("AdminTask.%s('%s',\n\t'%s')" % (str(self.adminTaskOp.__name__), str(parentArg), str(configData)))	
			childId = self.adminTaskOp(parentArg, configData)
		else:
			print ("AdminTask.%s(\n\t'%s')" % (str(self.adminTaskOp.__name__), str(configData)))
			childId = self.adminTaskOp(configData)

		# If AdminTask did not return the id, look it up
		result = None
		if not childId:
			result = dm.find(config.asObject(parentId))
		else:
			result = config.td.asObject(childId)
	
		# TODO Optimization: Only call modify if properties weren't processed by AdminTask
		dm.modify(result)
		
		return result
		
	def __str__(self):
		return "AdminTask.%s [useParentIdArg=%s, useParentScopeForFirstArg=%s]" % (str(self.adminTaskOp.__name__), str(useParentIdArg), str(useParentScopeForFirstArg))


##################################################################################
## Finders for child objects		
class Finder:
	def __init__(self, matchAttr):
		self.matchAttr = matchAttr

	def find(self, dm, parent):
		matchAttr = self.matchAttr
		matchValue = dm.data.get(matchAttr)
		utils.debug("matchAttr=%s matchValue=%s" % (str(matchAttr), str(matchValue)))
		if not matchValue:
			dm.show()
			raise "'%s' has no key attributed named '%s'" % (dm.getType(), matchAttr)
		if dm.isRootObject():
			result = config.find(dm.getType(), matchAttr, matchValue)
		else:
			result = parent.findChild(dm.getType(), matchAttr, matchValue)
		return result
			
defaultFinder = Finder("name")

class ClassloaderFinder(Finder):
	def __init__(self):
		Finder.__init__(self, "mode")
		
	def find(self, dm, parent):
		mode = dm.data.get("mode")
		if not mode:
			dm.data["mode"] = "PARENT_FIRST"
		return Finder.find(self, dm, parent)

class SIBusMember_Finder:
	def find(self, dm, parent):
		server = dm.data.get("server")
		cluster = dm.data.get("cluster")
		print "--> SIBusMember_Finder_find(%s\n\tserver=%s cluster=%s" % (str(parent.getId()), str(server), str(cluster))
		
		if server:
			return parent.findChild("SIBusMember", "server", server)
			
		if cluster:
			return parent.findChild("SIBusMember", "cluster", cluster)

		# TODO Handle other variations:  mqServer, ?
		
		
class J2EEResourcePropertySet_Finder:
	def find(self, dm, parent):
		return parent.child("J2EEResourcePropertySet")
	
	
class Referenceable_Finder:	
	def find(self, dm, parent):
		factoryClassname = dm.data.get("factoryClassname")
		classname = dm.data.get("classname")
		print "--> Referenceable_Finder_find(%s %s\n\tparent=%s" % (str(factoryClassname), str(classname), str(parent.getId()))
		for child in parent.children("Referenceable"):
			if (child.attr("factoryClassname") == factoryClassname) and (child.attr("classname") == classname):
				return child

##################################################################################
## CustomPropHandlers

class DataSource_CustomPropHandler:
	def handle(self, dm, result):
		customProps = dm.customProperties
		configureResourceProperties = []
		for key in customProps.keys():
			val = customProps.get(key)
			if key != "connectionProperties":
				#TODO Need configurable way to handle types
				configureResourceProperties.append([key, "java.lang.String", val])
		result["configureResourceProperties"] = configureResourceProperties



##################################################################################
## Dynamic initialization values
class JDBCProvider_CustomInitHandler:
	def handle(this, parentId, dm, result):
		idHelper = utils.IdHelper(parentId)
		result["scope"] = idHelper.getJDBCProviderAdminTask()

#TODO
class J2CResourceAdapter_CustomInitHandler:
	def handle(this, parentId, dm, result):
		idHelper = utils.IdHelper(parentId)
		result["nodeName"] = idHelper.getNode()

##################################################################################
# adminTaskMappings: Meta data to map canonical data to AdminTask format
#
# 	<ObjectType> = One of AdminConfig.types()
#		taskInit = properties to add to AdminTask when creating the object, never used by AdminConfig
#		configToTaskMapping = lhs is the AdminConfig name and rhs is the AdminTask name.  User data should always be AdminConfig name.  Only attributes defined here are mapped to AdminTask data.  During create, AdminConfig.modify is called immediate after AdminTask.customFactoryOperation
#		customPropMapping = lhs is the Custom Prop name and the rhs is the AdminTask name.  User data should always be AdminConfig name.  Only custom properties listed here will be added to AdminTask and must handle identity mapping
#		processor = The function that handling invoking AdminTask
#		<UseCase>
#			... All the same attributes under <ObjectType>
#
#
# <UseCase> exists when an <ObjectType>, like J2CActivationSpec, is too generic
# to define a complete mapping.  <UseCases>s for J2CActivationSpec include WMQActivationSpec
# and SIBUSActivationSpec

adminTaskMappings = {
	"Cell": {
		"rootObject": 1
	},
	"J2EEResourcePropertySet": {
		"finder": J2EEResourcePropertySet_Finder()
	},
	"Referenceable": {
		"finder": Referenceable_Finder()
	},
	"Classloader": {
		"finder": ClassloaderFinder()
	},
	"LibraryRef": {
		"finder": Finder("libraryName")
	},
	"JDBCProvider": {
		"configToTaskMapping": {
			"name": "name"
		},
		"processor": AT(AdminTask.createJDBCProvider, useParentIdArg=0),
		"customInitHandler": JDBCProvider_CustomInitHandler(),
		"impls": {
			"OracleXAJDBCProvider": {
				"taskInit": { 
					"databaseType": "Oracle",
					"providerType": "Oracle JDBC Driver",
					"implementationType": "XA data source"
				},
				"defaults": {
					"name": "Oracle JDBC Driver (XA)",
					"description": "Oracle JDBC Driver (XA)",
					"classpath": "${ORACLE_JDBC_DRIVER_PATH}/ojdbc6.jar"
				}
			},
			"OracleJDBCProvider": {
				"taskInit": {
					"databaseType": "Oracle",
					"providerType": "Oracle JDBC Driver",
					"implementationType": "Connection pool data source"
				},
				"defaults": {
					"name": "Oracle JDBC Driver",
					"description": "Oracle JDBC Driver",
					"classpath": "${ORACLE_JDBC_DRIVER_PATH}/ojdbc6.jar"
				}
			}
		}
	},
	"DataSource": {
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
			"datasourceHelperClassname": "dataStoreHelperClassName",
			"authDataAlias": "componentManagedAuthenticationAlias"
		},
		"processor": AT(AdminTask.createDatasource),
		"customPropHandler": DataSource_CustomPropHandler(),
		"impls": {
			"derby": {
				"defaults": {
					"datasourceHelperClassname": "com.ibm.websphere.rsadapter.DerbyDataStoreHelper"
				}
			},
			"Oracle11gDS": {
				"defaults": {
					"datasourceHelperClassname": "com.ibm.websphere.rsadapter.Oracle11gDataStoreHelper"
				}
			}
		}
	},
	"MQQueue": {
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
			"baseQueueName": "queueName",
			"baseQueueManagerName": "qmgr"
		},
		"processor": AT(AdminTask.createWMQQueue)
	},
	"MQQueueConnectionFactory": {
		"taskInit": {
			"type": "QCF"
		},
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
			"queueManager": "qmgrName",
			"transportType": "wmqTransportType",
			"wmqServerSvrconnChannel": "qmgrSvrconnChannel",
			"host": "qmgrHostname",
			"port": "qmgrPortNumber"
		},
		"processor": AT(AdminTask.createWMQConnectionFactory)
	},
	"SIBus": {
		"configToTaskMapping": {
			"name": "bus"
		},
		"processor": AT(AdminTask.createSIBus, useParentIdArg=0),
		"rootObject": 1
	},
	"SIBusMember": {
		"configToTaskMapping": {
			"server": "server",
			"node": "node"
		},
		"processor": AT(AdminTask.addSIBusMember, useParentIdArg=0),
		"finder": SIBusMember_Finder()
	},
	"SIBQueue": {
		"configToTaskMapping": {
			"server": "server",
			"node": "node",
			"identifier": "name"
		},
		"taskInit": {
			"type": "Queue"
		},
		"processor": AT(AdminTask.createSIBDestination, useParentIdArg=0),
		"finder": Finder("identifier")
	},
	"J2CActivationSpec": {
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
			"destinationJndiName": "destinationJndiName",
			"description": "description",
			"authenticationAlias": "authAlias"
		},
		"impls": {
			"WMQActivationSpec": {
				# Custom prop values required by AdminTask
				"customPropMapping": {
					"destinationType": "destinationType",
					"messageSelector": "messageSelector",
					"queueManager": "qmgrName",
					"transportType": "wmqTransportType",
					"channel": "qmgrSvrconnChannel",
					"hostName": "qmgrHostname",
					"port": "qmgrPortNumber"
					 
				},
				# Defaults required by AdminTask
				"defaults": {
					"destinationType": "javax.jms.Queue"
				},
				"processor": AT(AdminTask.createWMQActivationSpec)
			},
			"SIBJMSActivationSpec": {
				"customPropMapping": {
					"AutoStopSequentialMessageFailure": "autoStopSequentialMessageFailure",
					"ConsumerDoesNotModifyPayloadAfterGet": "consumerDoesNotModifyPayloadAfterGet",
					"ForwarderDoesNotModifyPayloadAfterSet": "forwarderDoesNotModifyPayloadAfterSet"
				},
				"processor": AT(AdminTask.createSIBJMSActivationSpec)
			}
		}
	},
	"J2CAdminObject": {
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
			"description": "description"
		},
		"impls": {
			"SIBJMSQueue": {
				"customPropMapping": {
					"BusName": "busName",
					"DeliveryMode": "deliveryMode",
					"QueueName": "queueName",
					"ReadAhead": "readAhead",
					"scopeToLocalQP": "scopeToLocalQP",
					"producerBind": "producerBind",
					"producerPreferLocal": "producerPreferLocal",
					"gatherMessages": "gatherMessages"
					
				},
				"processor": AT(AdminTask.createSIBJMSQueue, useParentScopeForFirstArg=1)
			}
		}
	},
	"J2CConnectionFactory": {
		"configToTaskMapping": {
			"name": "name",
			"jndiName": "jndiName",
		},
		"impls": {
			"SIBJMSQCF": {
				"taskInit": {
					"type": "Queue"
				},
				"processor": AT(AdminTask.createSIBJMSConnectionFactory, useParentScopeForFirstArg = 1)
			},
			"SIBJMSTCF": {
				"taskInit": {
					"type": "Topic"
				},
				"processor": AT(AdminTask.createSIBJMSConnectionFactory, useParentScopeForFirstArg = 1)
			},
			"SIBJMSCF": {
				"processor": AT(AdminTask.createSIBJMSConnectionFactory, useParentScopeForFirstArg = 1)
			}
		},
		"processor": AT(AdminTask.createJ2CConnectionFactory)
	},
	"JAASAuthData": {
		"finder": Finder("alias")
	},
	"J2CResourceAdapter": {
		"taskInit": {
			"rar.DeleteSourceRar": "false"
		},
		"configToTaskMapping": {
			"name": "rar.name",
			"description": "rar.desc",
			"classpath": "rar.classpath",
			"archivePath": "rar.archivePath",
			"isolatedClassLoader": "rar.isolatedClassLoader"
		},
		"customInitHandler": J2CResourceAdapter_CustomInitHandler(),
		"processor": AT(AdminTask.installResourceAdapter, useParentIdArg=0)
	},
}

def getDictionary(d, key):
	result = d.get(key)
	if result:
		return result
	return {}

def first(child, parent, key):
	result = child.get(key)
	if not result and parent:
		result = parent.data.get(key)
	return result

def mergeChildParent(child, parent, key):
	cmap = child.get(key)
	pmap = None
	if parent:
		pmap = parent.data.get(key)
	if not pmap:
		return cmap
	if not cmap:
		return pmap
	utils.merge(cmap, pmap)


class TypeHelper:
	def __init__(this, name, data, parent = None):
		this.name = name
		this.data = data
		this.parent = parent
		this.configToTaskMapping = mergeChildParent(data, parent, "configToTaskMapping")
		this.customPropMapping = mergeChildParent(data, parent, "customPropMapping")
		this.taskInit = mergeChildParent(data, parent, "taskInit")
		this.defaults = mergeChildParent(data, parent, "defaults")
		if not this.defaults:
			this.defaults = {}

		this.customPropHandler = first(data, parent, "customPropHandler")
		this.customInitHandler = first(data, parent, "customInitHandler")
		this.processor = first(data, parent, "processor")
		this.finder = first(data, parent, "finder")
		if not this.finder:
			this.finder = defaultFinder
		
		this.impls = {}
		impls = data.get("impls")
		if impls:
			for implName in impls.keys():
				implData = impls.get(implName)
				implHelper = TypeHelper(implName, implData, this)
				this.impls[implName] = implHelper
				
	def getImpl(this, name):
		result = this.impls.get(name)
		if not result:
			raise "Unable to get impl mapping '%s' because configured type '%s' does not have a mapping" % (name, this.name)
		return result	

	def getConfigToTaskMapping(this):
		'''Properties used in AdminConfig that should also be used in AdminTask.'''

		return this.configToTaskMapping
		
	def getCustomPropMapping(this):
		'''Custom properties used in AdminConfig that should also be used in AdminTask.'''
		return this.customPropMapping
		
	def getCustomPropHandler(this):
		'''Custom properties that need a special format for AdminTask.
		Example:  DataSource has wraps custom properties in a custom field
		'''
		return this.customPropHandler
		
	def getCustomInitHandler(this):
		'''AdminTask initialization parameters that are calculated in a custom callback.  
		Example:  DataSource has a special format for scope
		'''
		return this.customInitHandler	
	
	def getTaskInit(this):
		'''AdminTask initialization parameters that are static'''
		return this.taskInit
		
	def getDefaults(this):
		'''Adds default values to the data set, usually only used in Implementation mappings'''
		return this.defaults
		
	def getProcessor(this):
		'''A wrapper around AdminTask for normalized usage'''
		return this.processor
		
	def getFinder(this):
		'''Used to find objects form a parent'''
		return this.finder


typeHelpers = {}
rootObjects = []

for typeName in adminTaskMappings.keys():
	mapping = adminTaskMappings.get(typeName)
	typeHelpers[typeName] = TypeHelper(typeName, mapping)
	if mapping.get("rootObject"):
		rootObjects.append(typeName)

def getTypeHelper(name, impl):
	typeHelper = typeHelpers.get(name)
	if impl:
		if not typeHelper:
			raise "Type '%s' is not defined" % (name)
		typeHelper = typeHelper.getImpl(impl)
	return typeHelper

def getCopy(d, key):
	if not d:
		return {}
	result = d.get(key)
	#print "getCopy: " + str(result)
	if result:
		return copy.deepcopy(result)
	return {}
	
class DataManager:
	
	def __init__(self, userData):
		self.userData = copy.deepcopy(userData)
		self.data = copy.deepcopy(self.userData)
		typeName = self.data.get("_type")
		if not typeName:
			raise "_type is a required attribute and must represent an AdminConfig type"
		self.typeName = self.data["_type"]

		del self.data["_type"]
		self.typeMetaData = config_meta.getType(self.typeName)
		self.implType = self.data.get("_impl")
		if self.implType:
			del self.data["_impl"]
		
		defaults = self.getDefaults()
		for defaultKey in defaults.keys():
			if not self.data.get(defaultKey):
				self.data[defaultKey] = defaults[defaultKey]
		
		self.customProperties = self.data.get("_customProps")
		if self.customProperties:
			del self.data["_customProps"]
		else:
			self.customProperties = {}
		
		self.taskInit = self.data.get("_init")
		if self.taskInit:
			del self.data["_init"]
			
		configKeys = self.typeMetaData.getKeys()
		
		nameAttr = self.typeMetaData.getSimpleAttr("name")
		jndiNameAttr = self.typeMetaData.getSimpleAttr("jndiName")
		if nameAttr and not self.data.get("name") and jndiNameAttr:
			jndiName = self.data.get("jndiName")
			if jndiName:
				drv(self.data)
		
		for dataKey in self.data.keys():
			if not configKeys.contains(dataKey):
				#print "Moving %s to customProps" % dataKey
				self.customProperties[dataKey] = self.data[dataKey]
				del self.data[dataKey]
		
	def getTypeHelper(this):
		return getTypeHelper(this.typeName, this.implType)
	
	def getDefaults(this):
		typeHelper = this.getTypeHelper()
		result = None
		if typeHelper:
			result = typeHelper.getDefaults()
		if not result:
			result = {}
		return result
	
	def getType(self):
		return self.typeName
		
	def hasCustomProperties(self):
		return len(self.customProperties) > 0
		
	def show(self):
		print "DataManager: "
		print "\ttypeName:implType=%s:%s" % (str(self.typeName), str(self.implType))
		print "\tuserData=" + str(self.userData)
		print "\tdata=" + str(self.data)
		print "\tcustomProperties=" + str(self.customProperties)
		print "\thasCustomProperties=" + str(self.hasCustomProperties())
		print "\ttaskInit=" + str(self.taskInit)
		print "\ttypeMetaData=" + str(self.typeMetaData)
		print "\tforAdminTask=" + str(self.forAdminTask())
		print "\tforAdminConfig=" + str(self.forAdminConfig())

	def forAdminConfig(self):
		adminConfigData = self.createDataForAdminConfig()
		return utils.forAdminConfig(self.data)
		
	def createDataForAdminConfig(self):
		adminConfigData = copy.deepcopy(self.data)
		return adminConfigData

	def forAdminTask(self):
		adminTaskData = self.createDataForAdminTask()
		return utils.forAdminTask(adminTaskData)
		
	def createDataForAdminTask(self):
		result = copy.deepcopy(self.data)
		typeHelper = self.getTypeHelper()
		if typeHelper:
			self.doCreateDataForAdminTask(result, typeHelper)
		return result

	def doCreateDataForAdminTask(self, result, typeHelper):
		# Remove AdminConfig properties that do not have an AdminTask mapping
		configToTaskMapping = typeHelper.configToTaskMapping
		if configToTaskMapping:
			for key in result.keys():
				if not configToTaskMapping.get(key):
					del result[key]

		# Convert AdminConfig properties to AdminTask properties
		configToTaskMapping = typeHelper.configToTaskMapping
		if configToTaskMapping:
			for key in configToTaskMapping.keys():
				val = result.get(key)
				if val != None:
					del result[key]
					taskKey = configToTaskMapping[key]
					result[taskKey] = val

		# Add static/predefined AdminTask only parameters
		taskInit = typeHelper.taskInit
		if taskInit:
			for key in taskInit.keys():
				value = taskInit[key]
				result[key] = value
				
		# Add user init AdminTask only parameters
		taskInit = self.taskInit
		if taskInit:
			for key in taskInit.keys():
				value = taskInit[key]
				result[key] = value
					
		#Map custom property data to AdminTask data using rules
		customPropMapping = typeHelper.customPropMapping
		if customPropMapping:
			for key in customPropMapping.keys():
				val = self.customProperties.get(key)
				if val:
					mappedKey = customPropMapping[key]
					result[mappedKey] = val

		customPropHandler = typeHelper.customPropHandler
		if customPropHandler:
			customPropHandler.handle(self, result)
					
	def getAdminTaskProcessor(self):
		result = None
		typeHelper = self.getTypeHelper()
		if typeHelper:
			result = typeHelper.processor
		return result

	def isRootObject(self):
		return self.getType() in rootObjects

	def getFinder(self):
		result = None
		typeHelper = self.getTypeHelper()
		if typeHelper:
			result = typeHelper.finder
		if not result:
			result = defaultFinder
		return result

	def find(self, parent):
		finder = self.getFinder()
		return finder.find(self, parent)

	def getParentId(self, parent):
		if self.isRootObject():
			return None
		if not parent:
			raise "%s is not a root object, expected parent object for resolve operation" % (self.getType())
		return parent.getId()

	def resolve(self, parent=None):
		if utils.debugOn:
			print "--> DataManager.resolve\n\tparent: %s" % str(parent)
			self.show()
		result = self.find(parent)
		if result:
			self.modify(result)
		else:
			result = self.create(parent)
		return result
		
	def modify(self, owner):
		attrs = self.forAdminConfig()
		if self.typeName != "JAASAuthData":
			print ("AdminConfig.modify('%s',\n\t'%s')" % (str(owner.getId()), str(attrs)))
		#traceback.print_stack()
		AdminConfig.modify(owner.getId(), attrs)
		if self.hasCustomProperties():
			owner.customProperties(self)
		
	def create(self, parent):
		parentId = self.getParentId(parent)
		processor = self.getAdminTaskProcessor()
		result = None
		
		if processor:
			result = processor.adminTask(parentId, self)
		else:
			configData = self.forAdminConfig()
			if self.typeName != "JAASAuthData":
				print ("AdminConfig.create(%s,\n\t%s,\n\t%s)" % (str(self.typeName), str(parentId), str(configData)))
			result = config.td.asObject(AdminConfig.create(self.typeName, parentId, configData))
			
			if self.hasCustomProperties():
				result.customProperties(self)

		return result
