import config

cell = config.cell
cellName = cell.getName()
security = config.getSingleton("Security")

node = config.getSingleton("Node")
nodeName = node.getName()
node.parent = cell

server = config.getSingleton("Server")
serverName = server.getName()
server.parent = node

jvm = server.getJavaVirtualMachine()
traceService = server.getTraceService()