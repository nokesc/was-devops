import config
import utils

class ServerDecorator:
	def __init__(self, server, appHelper):
		self.server = server
		self.node = server.getNode()
		self.appHelper = appHelper
	
	def ListenerPort(self, data):
		return self.server.ListenerPort(data)
	
	def getSIBusName(self):
		return self.appHelper.getSIBus().getName()
	
	def SIBusMember(self, data):
		data["server"] = self.server.getName()
		data["node"] = self.server.getNode().getName()
		return data

	def SIBQueue(self, data):
		_init = utils.childDict(data, "_init")
		_init["server"] = self.server.getName()
		_init["node"] = self.node.getName()
		_init["bus"] = self.getSIBusName()
		return data
	
class ClusterDecorator:
	def __init__(self, cluster, appHelper):
		self.cluster = cluster
		self.appHelper = appHelper

	def ListenerPort(self, data):
		#TODO Loop through each Cluster member and create a ListenerPort
		raise "ListenerPort not yet supported by ClusterDecorator"

	def getSIBusName(self):
		return self.appHelper.getSIBus().getName()

	def SIBusMember(self, data):
		data["cluster"] = self.cluster.getName()
		return data

	def SIBQueue(self, data):
		_init = utils.childDict(data, "_init")
		_init["cluster"] = self.cluster.getName()
		_init["bus"] = self.getSIBusName()
		return data

decorators = {
	"Server": ServerDecorator,
	"Cluster": ClusterDecorator
}

class AppHelper:
	def __init__(self, scope, deployId, sibus=None, sibusMember=None, MDBHelper=None):
		self.scope = scope
		self.sibus = sibus
		self.deployId = deployId
		self.sibusMember = sibusMember
		self.sibusQCF = None
		self.decorator = decorators.get(scope.getConfigType())(scope, self)
		
		self.sibRA = scope.getSIBJMSResourceAdapter()
		self.mqProvider = scope.getWebSphereMQJMSProvider()
		self.mqRA = scope.getWebSphereMQResourceAdapter()
		self.urlProvider = scope.getDefaultURLProvider()
		self.genericJMSProvider = None
		
		self.mdbHelper = MDBHelper
		if not MDBHelper:
			self.mdbHelper = SIBusMDBHelper

		self.wmqProps = {
			"host": "${WMQ_host}",
			"port": "1414",
			"queueManager": "${WMQ_queueManager}",
			"XAEnabled": "true"
		}

	'''
	op: The operation to call for every element in list
	list: An array of data elements to resolve into WAS objects
	'''
	def resolveAll(self, op, list):
		result = []
		for data in list:
			result.append(op(data))
		return result

	def replaceDeployId(self, data, keys):
		#print "keys1=" + str(keys)
		#print str(type(keys))
		if not utils.isList(keys):
			keys = [keys]
		#print "keys2=" + str(keys)
		for key in keys:
			#print "key=" + str(key)
			value = data.get(key)
			if value:
				update = value.replace("${deployId}", self.deployId)
				#print "update: " + str(update)
				data[key] = update
			
	def jndiAndNameUpdate(self, data):
		self.replaceDeployId(data, ["jndiName", "name"])

	def URL(self, data):
		self.jndiAndNameUpdate(data)
		return self.urlProvider.URL(data)

	def TimerManagerInfo(self, data):
		self.jndiAndNameUpdate(data)
		return self.scope.getTimerManagerProvider().TimerManagerInfo(data)

	def WorkManagerInfo(self, data):
		self.jndiAndNameUpdate(data)
		return self.scope.getWorkManagerProvider().WorkManagerInfo(data)

	def ListenerPort(self, data):
		self.jndiAndNameUpdate(data)
		return self.decorator.ListenerPort(data)

	def getSIBus(self):
		busName = "main"
		if self.deployId:
			busName = self.deployId
		if not self.sibus:
			self.sibus = config.resolveSIBus({
				"name": busName
			})
		if not self.sibusMember:
			sibusMemberData = self.decorator.SIBusMember({
				"_init": {
					"bus": busName
				}
			})
			self.sibusMember = self.sibus.SIBusMember(sibusMemberData)
		return self.sibus

	def MDBHelper(self, name, queueName = None):
		return self.mdbHelper(self, name, queueName)
		
	def setJMSProvider(self, name):
		self.genericJMSProvider = self.scope.getJMSProvider(name)
	
	def JMSProvider(self, data):
		self.genericJMSProvider = self.scope.JMSProvider(data)
		print "AppHelper Setting genericJMSProvider context to "
		self.genericJMSProvider.show()
		return self.genericJMSProvider
		
	def GenericJMSDestination(self, data):
		self.jndiAndNameUpdate(data)
		return self.genericJMSProvider.GenericJMSDestination(data)
		
	def GenericJMSConnectionFactory(self, data):
		return self.genericJMSProvider.GenericJMSConnectionFactory(data)

	def getSIBusQCF(self):
		if not self.sibusQCF:
			sibus = self.getSIBus()
			self.sibusQCF = self.SIBJMSQCF({"jndiName": "%s/jms/qcf" % (self.deployId)})
		return self.sibusQCF

	def SIBQueue(self, data):
		self.jndiAndNameUpdate(data)
		sibQueueData = self.decorator.SIBQueue(data)
		return self.getSIBus().SIBQueue(sibQueueData)		
		
	def SIBJMSQCF(self, data):
		self.jndiAndNameUpdate(data)
		_init = utils.childDict(data, "_init")
		_init["busName"] = self.getSIBus().getName()
		return self.sibRA.SIBJMSQCF(data)

	def SIBJMSQueue(self, data):
		self.jndiAndNameUpdate(data)
		_init = utils.childDict(data, "_init")
		_init["busName"] = self.getSIBus().getName()
		return self.sibRA.SIBJMSQueue(data)
		
	def SIBJMSActivationSpec(self, data):
		self.jndiAndNameUpdate(data)
		_init = utils.childDict(data, "_init")
		_init["busName"] = self.getSIBus().getName()
		return self.sibRA.SIBJMSActivationSpec(data)
		
	def MQQueue(self, data):
		self.jndiAndNameUpdate(data)
		return self.mqProvider.MQQueue(data)
	
	def MQQueueConnectionFactory(self, data):
		self.jndiAndNameUpdate(data)
		utils.merge(data, self.wmqProps)
		return self.mqProvider.MQQueueConnectionFactory(data)
		
	def WMQActivationSpec(self, data):
		self.jndiAndNameUpdate(data)
		utils.merge(data, {
			"hostName": self.wmqProps["host"],
			"port": self.wmqProps["port"],
			"queueManager": self.wmqProps["queueManager"]
		})
		return self.mqRA.WMQActivationSpec(data)
		
	def ResourceEnvironmentProvider(self, data):
		utils.merge(data, {
			"_type": "ResourceEnvironmentProvider"
		})
		return self.scope.resolve(data)
		
	def resolve(self, data):
		self.jndiAndNameUpdate(data)
		return self.scope.resolve(data)

class WMQMDBHelper:
	def __init__(self, appHelper, name, queueName = None):
		self.appHelper = appHelper
		self.name = name
		self.queueName = queueName
		if not self.queueName:
			self.queueName = name
		
	def createSolution(self):
		#TODO ListenerPort vs ActivationSpec option
		#TODO Configure error queue
		name = self.name
		deployId = self.appHelper.deployId
		queueName = self.queueName
		queueJndiName = "%s/jms/queue/%s" % (deployId, queueName)
		qcfJndiName = "%s/jms/qcf/%s" % (deployId, name)
		asJndiName = "%s/jms/as/%s" % (deployId, name)

		self.appHelper.MQQueue({
			"jndiName": queueJndiName,
			"baseQueueName": queueName
		})

		#self.appHelper.MQQueueConnectionFactory({
		#	"jndiName": qcfJndiName,
		#	"XAEnabled": "true"	
		#})
		
		self.appHelper.WMQActivationSpec({
			"jndiName": asJndiName,
			"destinationJndiName": queueJndiName
		})

class SIBusMDBHelper:
	def __init__(self, appHelper, name, queueName = None):
		self.appHelper = appHelper
		self.name = name
		self.queueName = name
		if queueName:
			self.queueName = queueName
		
	def createSolution(self):
		# TODO Configure error queue
		name = self.name
		queueName = self.queueName
		deployId = self.appHelper.deployId
		queueJndiName = "%s/jms/queue/%s" % (deployId, queueName)
		qcfJndiName = "%s/jms/qcf/%s" % (deployId, name)
		asJndiName = "%s/jms/as/%s" % (deployId, name)
		
		self.appHelper.SIBQueue({
			"identifier": queueName,
		})
		
		self.appHelper.SIBJMSQueue({
			"jndiName": queueJndiName,
			"QueueName": queueName
		})
		
		#self.appHelper.SIBJMSQCF({
		#	"jndiName": qcfJndiName
		#})
		
		self.appHelper.SIBJMSActivationSpec({
			"jndiName": asJndiName,
			"destinationJndiName": queueJndiName
		})
