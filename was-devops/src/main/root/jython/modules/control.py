import java.util.Date as Date

dataSourceCfgHelperId = AdminControl.queryNames("WebSphere:*,type=DataSourceCfgHelper")
AdminControl.invoke(dataSourceCfgHelperId, "reload")
def reload():
	print str(Date()) + " reload ... " +  dataSourceCfgHelperId
	print AdminControl.invoke(dataSourceCfgHelperId, "reload")
	print str(Date()) + " reload complete "