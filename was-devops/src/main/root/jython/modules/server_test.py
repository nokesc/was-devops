import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server
mls = server.getMessageListenerService()

class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()

	def tearDown(self):
		AdminConfig.reset()

	def validateResolve(self, parent, data):
		if parent:
			print "validateResolve: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
	
	def test_DerbyJDBCProvider(self):
		jp = server.getDerbyJDBCProvider()
		self.assertNotEquals(None, jp)
		print jp.getId()

	def test_ListenerPort(self):
		listenerPort = {
			"_type": "ListenerPort",
			"name": "LP1",
			"description": "my listener port",
			"connectionFactoryJNDIName": "jms/qcf",
			"destinationJNDIName": "jms/q1",
			"maxMessages": "1",
			"maxRetries": "2",
			"maxSessions": "10"
		}
		mls.remove(listenerPort)
		
		self.validateResolve(mls, listenerPort)

	def test_Library(self):
		library = {
			"_type": "Library",
			"name": "ut_lib",
			"classPath": ["abc", "def"],
			"description": "My Lib",
			"isolatedClassLoader": "false"
			
		}
		server.remove(library)
		
		self.validateResolve(server, library).show()
		
	def test_Classloader_LibraryRef(self):
		libraryName = "mylib1"

		library = server.Library({
			"name": libraryName,
			"classPath": "abc.jar;def.jar",
		})
		
		as = server.getApplicationServer()
		classloader = self.validateResolve(as, {
			"_type": "Classloader",
			"mode": "PARENT_FIRST"
		})
		libraryRef = self.validateResolve(classloader, {
			"_type": "LibraryRef",
			"libraryName": libraryName
		})
		
	def test_getJDBCProviderAdminTask(self):
		scopeName = server.getIdHelper().getJDBCProviderAdminTask()
		self.assertEquals("Node=%s,Server=%s" % (node.getName(), server.getName()), scopeName) 
