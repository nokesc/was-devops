import utils
import unittest

ID1 = "\"Default URL Provider(cells/Chris-THINKNode01Cell/nodes/Chris-THINKNode01/servers/server1|resources.xml#URLProvider_1)\"";
ID2 = "(cells/Chris-THINKNode01Cell|security.xml#CustomUserRegistry_1)";
ID3 = "readAhead(cells/Chris-THINKNode01Cell|resources.xml#J2EEResourceProperty_1402160561938)";

class IdHelperTest3(unittest.TestCase):
	
	def test_idWithQuote(self):
		helper = utils.IdHelper(ID1)
		self.assertEquals("Chris-THINKNode01Cell", helper.getCell())
		self.assertEquals("URLProvider", helper.getType())
		self.assertEquals("Default URL Provider", helper.getName())
		self.assertEquals("/Cell:Chris-THINKNode01Cell/Node:Chris-THINKNode01/Server:server1/",
				helper.getScopeContainment())
	

	def test_idNoName(self):
		helper = utils.IdHelper(ID2)
		self.assertEquals("Chris-THINKNode01Cell", helper.getCell())
		self.assertEquals("CustomUserRegistry", helper.getType())
		self.assertEquals(None, helper.getName())
		self.assertEquals("/Cell:Chris-THINKNode01Cell/", helper.getScopeContainment())

	def idStandard(self):
		helper = utils.IdHelper(ID3)
		self.assertEquals("Chris-THINKNode01Cell", helper.getCell())
		self.assertEquals("J2EEResourceProperty", helper.getType())
		self.assertEquals("readAhead", helper.getName())
		self.assertEquals("/Cell:Chris-THINKNode01Cell/", helper.getScopeContainment())
