import base
import config
import data_manager as dm
import unittest
import utils
import java.lang.NullPointerException

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server

class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()

	def tearDown(self):
		AdminConfig.reset()

	def validateResolve(self, parent, data):
		if parent:
			print "validateResolve: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
	
##	def test_1(self):	
##		self.assertEquals(1, 2)
		
##	def test_2(self):	
##		self.assertEquals(1, 1)

##	def test_3(self):	
##		raise java.lang.NullPointerException()

	def tes_ResourceEnvironmentProvider(self):
		rep = {
			"_type": "ResourceEnvironmentProvider",
			"name": "rep_UT",
			"_customProps": {
				"cust1": "val1",
				"cust2": "val2"
			}
		}
		repObj = self.validateResolve(server, rep)
		
		ref = {
			"_type": "Referenceable",
			"factoryClassname": "a",
			"classname": "b"
		}
		refObj = self.validateResolve(repObj, ref)
		
		ree = {
			"_type": "ResourceEnvEntry",
			"name": "ree1",
			"jndiName": "ree1",
			"referenceable": refObj.getId(),
			"_customProps": {
				"cust3": "val3",
				"cust4": "val4"
			}
		}
		reeObj = self.validateResolve(repObj, ree)
