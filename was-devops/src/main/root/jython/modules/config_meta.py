import utils

import java.lang.String as String
import java.lang.StringBuilder as SB

import java.util.ArrayList as ArrayList
import java.util.HashSet as HashSet
import java.util.LinkedHashMap as LinkedHashMap
import java.util.regex.Pattern as Pattern

simpleTypes = utils.toSet("String", "Boolean", "boolean", "Integer", "int", "Float", "float", "Character", "char", "Double", "double", "Long", "long", "ENUM")

attrPattern = Pattern.compile("(\S+)\s+([\w\.]+)(\*)?(?:\((.+)\))?(@)?(\*)?")

readOnly = {"DataSource": utils.toSet("providerType") }

def getReadOnlyAttrs(typeName):
	result = HashSet()
	if typeName in readOnly.keys():
		result = readOnly[typeName]
	return result
	

class TypeRepo:
	def __init__(self):
		self.types = LinkedHashMap()
		typeNames = AdminConfig.types().splitlines()
		utils.debug("TypeRepo: number of types %s" % len(typeNames))
		for typeName in typeNames:
			self.types.put(typeName, TypeMetaData(typeName))
			
	def getTypes(self):
		return self.types.values()

	def getType(self, name):
		return self.types.get(name)

class TypeMetaData:
	def __init__(self, typeName):
		self.name = typeName
		self.all = LinkedHashMap()
		self.simpleAttrs = LinkedHashMap()
		self.compositeAttrs = LinkedHashMap()
		self.simpleAttrsAsString = ""
		self.initialized = 0
		
	def __doInit(self):
		if not self.initialized:
			# TODO Lazy init
			attrMetaBlob = AdminConfig.attributes(self.name)
			readOnlySet = getReadOnlyAttrs(self.name)
			for line in attrMetaBlob.splitlines():
				utils.debug(line)
				if 1:
					matcher = attrPattern.matcher(String(line))
					if matcher.matches():
						utils.debug("\t\t1:%s 2:%s 3:%s 4:%s 5:%s 6:%s" %  (matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5), matcher.group(6)))
						multi = matcher.group(3)
						if not multi:
							multi = matcher.group(6)
						options = matcher.group(4)
						if options:
							options = options.split(", ")
						data = {"name": matcher.group(1), "type": matcher.group(2), "multi": multi, "options": options, "reference": matcher.group(5) }						
						utils.debug(data)
						
						attr = AttributeMetaData(self, data)
						
						self.all.put(data['name'], attr)
						if simpleTypes.contains(data['type']):
							self.simpleAttrs.put(data['name'], attr)
						else:
							self.compositeAttrs.put(data['name'], attr)

						if readOnlySet.contains(data['name']):
							attr.readOnly = "readOnly"
						else:
							attr.readOnly = 0 
						
					else:
						utils.debug("\t\tno match")
						raise Exception(self.name + ", line=[" + line + "]")
					
			
			for value in self.simpleAttrs.values():
				if len(self.simpleAttrsAsString) > 0:
					self.simpleAttrsAsString += " "
				self.simpleAttrsAsString += value.name				
			

	def getAllAttrs(self):
		self.__doInit()
		return self.all

	def getSimpleAttrs(self):
		self.__doInit()
		return self.simpleAttrs

	def getSimpleAttr(self, name):
		return self.getSimpleAttrs().get(name)

	def getSimpleAttrsAsString(self):
		self.__doInit()
		return self.simpleAttrsAsString
		
	def __str__(self):
		return str(self.name)
	
	def __repr__(self):
		return self.__str__()
		
	def getTemplate(self):
		buf = SB()
		buf.append("{")
		count = 0
		for attr in self.getSimpleAttrs().values():
			if count > 0:
				buf.append(",\n")
			else:
				buf.append("\n")
			count = count + 1
			buf.append('\t"').append(attr.name).append('": ')
			if attr.multi:
				buf.append("[]")
			else:
				buf.append('""')
		buf.append("\n}")
		return buf.toString()
		
	def show(self):
		print self
		for attr in self.getAllAttrs().values():
			print "\t" + str(attr)

	def getKeys(self):
		self.__doInit()
		return self.all.keySet()

class AttributeMetaData:
	def __init__(self, owner, data):
		self.owner = owner
		self.name = data['name']
		self.type = data['type']
		self.multi = data['multi']
		self.ref = data['reference']
		self.options = data['options']

	def isReadOnly():
		return self.readOnly

	def __str__(self):
		multi = self.multi
		if not multi:
			multi = 1
		ref = self.ref
		if not ref:
			ref = ""
		readOnly = ""
		if self.readOnly:
			readOnly = " " + str(self.readOnly)
			
		options = ""
		if self.options:
			options = " " + str(self.options)
		
		return "%s.%s %s[%s]%s%s%s" % (self.owner, self.name, self.type, multi, ref, readOnly, options)
		
	def __repr__(self):
		return self.__str__()

typeRepo = TypeRepo()

def getTypes():
	return typeRepo.getTypes()
	
def listTypes():
	for t in getTypes():
		print t	

def getType(name):
	return typeRepo.getType(name)
	
def show(*args):
	for arg in args:
		t = getType(arg)
		t.show()
		
def showWithTemplate(*args):
	for arg in args:
		t = getType(arg)
		t.show()
		print t.getTemplate()
		
def templates(*args):
	for arg in args:
		t = getType(arg)
		print t.name
		print t.getTemplate()