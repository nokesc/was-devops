import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server
derbyProvider = server.getDerbyJDBCProvider()


class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()

	def tearDown(self):
		AdminConfig.reset()

	def validateCreation(self, parent, data):
		if parent:
			print "validateCreation: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
	def test_derbyDataSource(self):
		derbyDS = {
			"_type": "DataSource",
			"_impl": "derby",
			"name": "derbyUT",
			"jndiName": "jdbc/derbyUT",
			"_customProps": {
				"databaseName": "db1"
			}
		}
		self.validateCreation(derbyProvider, derbyDS)
