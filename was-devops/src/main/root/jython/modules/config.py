import utils
import config_meta
import copy
import data_manager
import traceback

import java.lang.String as String

DataManager = data_manager.DataManager

def resolveAll(op, list):
	result = []
	for data in list:
		result.append(op(data))
	return result	

nodes = {}

def getScope(idHelper):
	scopeId = self.getIdHelper().getScopeId()
	return asObject(scopeId)

def getNode(name):
	node = nodes.get(name)
	if not node:
		node = find("Node", "name", name)
		nodes[name] = node
	return node

def getId(type):
	return AdminConfig.getid("/%s:/" % (type))
	
def getSingleton(type):
	return asObject(getId(type))

def attr(id, name):
	return AdminConfig.showAttribute(id, name)

def listIds(typeName, scope=None):
	if scope:
		result = AdminConfig.list(typeName, scope)
		result = result.splitlines()
		return result
	return AdminConfig.list(typeName).splitlines()
	
def listId(typeName, scope=None):
	ids = listIds(typeName, scope)
	if len(ids) > 0:
		return ids[0]
	return None
	
def findId(typeName, matchAttr, matchValue, scope=None):
	if scope:
		ids = AdminConfig.list(typeName, scope).splitlines()
	else:
		ids = AdminConfig.list(typeName).splitlines()
	return match(ids, matchAttr, matchValue)


def find(typeName, matchAttr, matchValue, scope=None):
	id = findId(typeName, matchAttr, matchValue, scope)
	if id:
		return asObject(id)

def match(ids, matchAttr, matchValue):
	for id in ids:
		if attr(id, matchAttr) == matchValue:
			return id

def listObjects(typeName):
	result = []
	for configId in listIds(typeName):
		result.append(asObject(configId))
	return result
	
def resolveSIBus(data):
	data["_type"] = "SIBus"
	dm = DataManager(data)
	return dm.resolve()
	
def getSIBus(name):
	return find("SIBus", "name", name)
	
class Attributes:
	def __init__(self, obj, data):
		self.obj = obj
		self.data = data
		
	def __str__(self):
		return str(self.data)
		
	def show(self):
		print self.obj.getConfigType()
		print str(self.data)
		for item in self.data.items():
			print "\t%s: %s" % (item[0], item[1])

	def modify(self):
		configMetaData = self.obj.getConfigMetaData()
		for readOnly in config_meta.getReadOnlyAttrs(configMetaData.name):
			self.data.remove(readOnly)
		value = utils.forAdminConfig(self.data)
		AdminConfig.modify(self.obj.getId(), value)


class TypesDictionary:
	def __init__(self):
		self.types = {}
		
	def register(self, cl):
		self.types[cl.__name__] = cl
		
	def asObject(self, id):
		typeName = AdminConfig.getObjectType(id)
		cl = self.types.get(typeName)
		if not cl:
			cl = Object
		return cl(id)
		
	def getType(self, typeName):
		return self.types.get(typeName)
		
td = TypesDictionary()
	
def asObject(id):
	return td.asObject(id)

class Object:
	'''An object representing an AdminConfig.type.
	'''
	def __init__(self, id):
		self.id = id
		self.idHelper = None
		self.scope = None
		configTypeName = AdminConfig.getObjectType(id)
		self.configMetaData = config_meta.getType(configTypeName)
		utils.debug("Created Object for " + str(id))

	def getIdHelper(self):
		if not self.idHelper:
			idHelper = utils.IdHelper(self.getId())
		return idHelper
	
	def getScope(self):
		if not self.scope:
			self.scope = getScope(self.getIdHelper())
		return self.scope

	def getId(self):
		return self.id
		
	def removeChildren(self, typeName):
		for child in self.children(typeName):
			child.removeSelf()

	def removeSelf(self):
		# TODO find AdminTask.remove method
		print "AdminConfig.remove('%s')" % (str(self.getId()))
		AdminConfig.remove(self.getId())

	def getConfigMetaData(self):
		return self.configMetaData

	def getConfigType(self):
		return self.configMetaData.name

	def attr(self, name):
		#print "attr: %s %s" % (self.id, name)
		return attr(self.id, name)

	def simpleAttrs(self):
		configMetaData = self.configMetaData
		attrBlob = AdminConfig.show(self.id, configMetaData.getSimpleAttrsAsString())
		result = utils.attrBlobToMap(attrBlob)
		return Attributes(self, result)
		
	def setAttr(self, attrName, value):
		self.modify({attrName: value})

	def modify(self, data):
		attrs = utils.forAdminConfig(data)
		if self.getConfigType() != "JAASAuthData":
			print "AdminConfig.modify('%s',\n\t'%s')" % (str(self.id), str(attrs))
		AdminConfig.modify(self.id, attrs)
	
	def show(self):
		print self.getConfigMetaData().name
		attrs = self.simpleAttrs()
		data = attrs.data
		for key in data.keys():
			value = data[key]
			print "\t%s: %s" % (key, value)
			
	def children(self, typeName):
		result = []
		for id in listIds(typeName, self.id):
			result.append(asObject(id))
		return result
		
	def child(self, typeName):
		ids = listIds(typeName, self.id)
		for id in ids:
			return asObject(id)
		
	def findChild(self, typeName, matchAttr, matchValue):
		id = findId(typeName, matchAttr, matchValue, self.id)
		if id:
			return asObject(id)
	
	def asChild(self, data):
		dm = DataManager(data)
		return dm.find(self)

	def findAndRemove(self, typeName, matchAttr, matchValue):
		child = self.findChild(typeName, matchAttr, matchValue)
		if child:
			child.removeSelf()
		else:
			print "Unable to remove, no match found for %s %s=%s" % (typeName, matchAttr, matchValue)
	
	def remove(self, data):
		dm = DataManager(data)
		child = dm.find(self)
		if child:
			child.removeSelf()
	
	def resolve(self, data, _type = None):
		if _type:
			data["_type"] = _type
		if not data.get("_type"):
			raise "_type is a required attribute when calling resolve: " + str(data)
		dm = DataManager(data)
		return dm.resolve(self)

	def customProperties(self, dm):
		raise "customProperties not supported by type %s[%s]\n\t%s" % (self.getConfigType(), str(self.__class__.__name__), str(dm.customProperties))


	def getJ2EEResourceProperties(self):
		resourceProperties = {}
		for prop in self.children("J2EEResourceProperty"):
			resourceProperties[prop.getName()] = prop
		return resourceProperties
			
	def customPropsToJ2EEResourceProperties(self, dm):
		print "%s#customPropsToJ2EEResourceProperties %s" % (self.getConfigType(), str(dm.customProperties))
		#traceback.print_stack()
		resourceProperties = self.getJ2EEResourceProperties()
		customProperties = dm.customProperties
		for key in customProperties.keys():
			prop = resourceProperties.get(key)
			if not prop:
				raise "Unable to find prop named '%s' for type %s" % (key, self.getConfigType())
			value = customProperties.get(key)
			prop.setValue(value)
			
	def customPropsUpdateJ2EEResourceProperties(self, dm):
		print "%s#customPropsUpdateJ2EEResourceProperties %s" % (self.getConfigType(), str(dm.customProperties))
		resourceProperties = self.getJ2EEResourceProperties()
		customProperties = dm.customProperties
		for key in customProperties.keys():
			prop = resourceProperties.get(key)
			if not prop:
				data = {
					"name": key,
					"value": customProperties[key]
				}
				configData = utils.forAdminConfig(data)
				print "AdminConfig.create('J2EEResourceProperty', " + str(self.getId()) + ", " + str(configData) + ")"
				AdminConfig.create("J2EEResourceProperty", self.getId(), configData)
			else:
				value = customProperties.get(key)
				prop.setValue(value)

	def J2EEResourcePropertySet(self):
		return self.resolve({}, _type="J2EEResourcePropertySet")
			
	
class _Resource(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def customProperties(self, dm):		
		self.customPropsUpdateJ2EEResourceProperties(dm)
		
	def getName(self):
		return self.attr("name")

class Scope(Object):
	def __init__(self, id):
#TODO Initialize self.containment
		Object.__init__(self, id)
		self.name = self.attr("name")
	
	def getName(self):	
		return self.name
		
	def getContainmentPath(self):
		# TODO Link into constructor
		return self.getIdHelper().getScopeContainment()

	def getVariableMap(self):
		id = AdminConfig.getid(self.getContainmentPath() + "VariableMap:/")
		if not id:
			raise "Unable to find VariableMap for " + self.configTypeName + ":" + self.getName()
		return td.asObject(id)
		
	def setVars(self, values):
		variableMap = self.getVariableMap()
		variableMap.setVars(values)
		
	def setVarsFromProps(self, envName):
		envProps = utils.serverLibProps("environment/%s.env.properties" % (envName))
		self.setVars(envProps)
		
	def showVars(self):
		self.getVariableMap().show()
		
	def debug(self):
		print self.getName()
		print "\tcontainment=" + self.containment
		print "\tadminTaskScope=" + self.adminTaskScope
		print "\tparent=" + str(self.parent)
			
	def Library(self, data):
		return self.resolve(data, _type="Library")

	def JDBCProvider(self, data):
		return self.resolve(data, _type="JDBCProvider")
		
	def getDerbyJDBCProvider(self):
		return find("JDBCProvider", "name", "Derby JDBC Provider")	
	
	def getOracleXAJDBCProvider(self):
		return self.getJDBCProvider("Oracle JDBC Driver (XA)")

	def getOracleJDBCProvider(self):
		return self.getJDBCProvider("Oracle JDBC Driver")
	
	def OracleXAJDBCProvider(self, data):
		data["_impl"] = "OracleXAJDBCProvider"
		_init = utils.childDict(data, "_init")
		_init["scope"] = self.getIdHelper().getJDBCProviderAdminTask()
		return self.JDBCProvider(data)
		
	def OracleJDBCProvider(self, data):
		data["_impl"] = "OracleJDBCProvider"
		_init = utils.childDict(data, "_init")
		_init["scope"] = self.getIdHelper().getJDBCProviderAdminTask()
		return self.JDBCProvider(data)

	def getJDBCProvider(self, name):
		return self.findChild("JDBCProvider", "name", name)
		
	def JMSProvider(self, data):
		return self.resolve(data, _type="JMSProvider")		

	def getJMSProvider(self, name):
		return self.findChild("JMSProvider", "name", name)
	
	def getWebSphereMQJMSProvider(self):
		return self.getJMSProvider("WebSphere MQ JMS Provider")
		
	def getJ2CResourceAdapter(self, name):
		return self.findChild("J2CResourceAdapter", "name", name)
		
	def getSIBJMSResourceAdapter(self):
		return self.getJ2CResourceAdapter("SIB JMS Resource Adapter")
		
	def getWebSphereMQResourceAdapter(self):
		return self.getJ2CResourceAdapter("WebSphere MQ Resource Adapter")
		
	def getURLProvider(self, name):
		return self.findChild("URLProvider", "name", name)	
	
	def getDefaultURLProvider(self):
		return self.getURLProvider("Default URL Provider")
	
	def ResourceEnvironmentProvider(self, data):
		return self.resolve(data, "ResourceEnvironmentProvider")
	
	def getTimerManagerProvider(self):
		return self.findChild("TimerManagerProvider", "name", "TimerManagerProvider")
		
	def getWorkManagerProvider(self):
		return self.findChild("WorkManagerProvider", "name", "WorkManagerProvider")
td.register(Scope)


class Server(Scope):
	def __init__(self, id):
		Scope.__init__(self, id)

	def getNode(self):
		return getNode(self.getIdHelper().getNode())
	
	def getMessageListenerService(self):
		return self.child("MessageListenerService")
		
	def ListenerPort(self, data):
		return self.getMessageListenerService().ListenerPort(data)
		
	def getJavaVirtualMachine(self):
		return self.child("JavaVirtualMachine")
		
	def getJVM(self):
		return self.getJavaVirtualMachine()
		
	def getApplicationServer(self):
		return self.child("ApplicationServer")

	def getTraceService(self):
		return self.child("TraceService")	
td.register(Server)


class ApplicationServer(Object):
	def __init__(self, id):
		Object.__init__(self, id)
	
	def Classloader(self, data):
		return self.resolve(data, _type="Classloader")
td.register(ApplicationServer)


class Classloader(Object):
	def __init__(self, id):
		Object.__init__(self, id)
	
	def LibraryRef(self, data):
		return self.resolve(data, _type="LibraryRef")
td.register(Classloader)


class LibraryRef(Object):
	def __init__(self, id):
		Object.__init__(self, id)
td.register(LibraryRef)		


class MessageListenerService(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def ListenerPort(self, data):
		return self.resolve(data, _type="ListenerPort")
		
	def getThreadPool(self):
		return self.child("ThreadPool")
td.register(MessageListenerService)


class Node(Scope):
	def __init__(self, id):
		Scope.__init__(self, id)
		nodes[self.getName()] = self
		self.keyStoreScope = '(cell):%s:(node):%s' % (cell.getName(), self.getName())
		
	def getServer(self, name):
		return findChild("Server", "name", name)
		
	def listTrustStore(self):
		return listSignerCertificates('NodeDefaultTrustStore', self.keyStoreScope)
	
	def addTrustCertificate(self, alias, path, base64Encoded):
		result = self.listTrustStore()
		print str(result)
		if not result.get(alias):
			print "Adding " + alias
			addSignerCertificate('NodeDefaultTrustStore', self.keyStoreScope, alias, path, base64Encoded)
			return 1
		else:
			return 0
			
	def deleteTrustCertificate(self, alias):
		result = self.listTrustStore()
		if result.get(alias):
			deleteSignerCertificate('NodeDefaultTrustStore', self.keyStoreScope, alias)
			return 1
		else:
			return 0

td.register(Node)

def listSignerCertificates(keyStoreName, keyStoreScope):
	certs = AdminTask.listSignerCertificates(['-keyStoreName', keyStoreName, '-keyStoreScope', keyStoreScope]).splitlines()
	result = {}
	for cert in certs:
		attrs = utils.parseAdminTaskAttrs(cert)
		alias = attrs.get("alias")
		result[alias] = attrs
	return result

def addSignerCertificate(keyStoreName, keyStoreScope, certificateAlias, certificateFilePath, base64Encoded):
	return AdminTask.addSignerCertificate('[-keyStoreName %s -keyStoreScope %s -certificateAlias %s -certificateFilePath %s -base64Encoded %s]' % (keyStoreName, keyStoreScope, certificateAlias, certificateFilePath, base64Encoded))

def deleteSignerCertificate(keyStoreName, keyStoreScope, certificateAlias):
	AdminTask.deleteSignerCertificate('[-keyStoreName %s -keyStoreScope %s -certificateAlias %s]' % (keyStoreName, keyStoreScope, certificateAlias))

class Cell(Scope):
	def __init__(self, id):
		Scope.__init__(self, id)

	def getCluster(self, name):
		return findChild("Cluster", "name", name)

	def getApplication(self, name):
		return findChild("Application", "name", name)

	def getNode(self, name):
		return getNode(name)
	
	def SIBus(self, data):
		data["_type"] = "SIBus"
		return self.resolve(data)
td.register(Cell)


class SIBus(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		self.name = self.attr("name")

	def SIBQueue(self, data):
		return self.resolve(data, _type="SIBQueue")
		
	def SIBusMember(self, data):
		return self.resolve(data, _type="SIBusMember")
		
	def getName(self):
		return self.name
td.register(SIBus)


class SIBusMember(Object):
	def __init__(self, id):
		Object.__init__(self, id)	
td.register(SIBusMember)


class Property(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def setValue(self, value):
		self.setAttr("value", value)
		
	def getName(self):
		return self.attr("name")
td.register(Property)


class JavaVirtualMachine(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def systemProperties(self, data):
		propMap = {}
		for prop in self.children("Property"):
			propMap[prop.getName()] = prop
		propKeySet = propMap.keys()
		for key, value in data.items():
			if key in propKeySet:
				prop = propMap[key]
				prop.setValue(value)
			else:
				data = {"name": key, "value": value}
				configData = utils.forAdminConfig(data)
				AdminConfig.create("Property", self.getId(), configData)
td.register(JavaVirtualMachine)


class URLProvider(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def URL(self, data):
		return self.resolve(data, _type="URL")
		
td.register(URLProvider)


class JDBCProvider(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def DataSource(self, data):
		return self.resolve(data, "DataSource")
		
		
	def Oracle11gDS(self, data):
		data["_impl"] = "Oracle11gDS"
		# Version 8.5 only? 
		#if not data.get("containerManagedPersistence"):
			#data["containerManagedPersistence"] = "false"
		return self.DataSource(data)
td.register(JDBCProvider)


class DataSource(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)

	def customProperties(self, dm):
		ps = self.J2EEResourcePropertySet()
		ps.customProperties(dm)
td.register(DataSource)


class ResourceEnvironmentProvider(Object):
	def __init__(self, id):
		Object.__init__(self, id)

	def Referenceable(self, data):
		return self.resolve(data, _type="Referenceable")
		
	def ResourceEnvEntry(self, data):
		return self.resolve(data, _type="ResourceEnvEntry")

	def customProperties(self, dm):
		ps = self.J2EEResourcePropertySet()
		ps.customProperties(dm)
td.register(ResourceEnvironmentProvider)


class Referenceable(Object):
	def __init__(self, id):
		Object.__init__(self, id)
td.register(Referenceable)


class ResourceEnvEntry(_Resource):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def customProperties(self, dm):
		ps = self.J2EEResourcePropertySet()
		ps.customProperties(dm)
td.register(ResourceEnvEntry)


class VariableMap(Object):
	def __init__(self, id):
		Object.__init__(self, id)
	
	def setVars(self, values):
		for key in values.keys():
			val = values[key]
			vse = self.findChild("VariableSubstitutionEntry", "symbolicName", key)
			if vse:
				print "setVars updating %s=%s" % (key, val)
				vse.setAttr("value", val)
			else:
				print "setVars creating %s=%s" % (key, val)
				data = {"symbolicName": key, "value": val}
				AdminConfig.create("VariableSubstitutionEntry", self.id, utils.forAdminConfig(data))
	
	def show(self):
		print self.getConfigMetaData().name
		for child in self.children("VariableSubstitutionEntry"):
			print '"%s": "%s"\n%s' % (child.attr("symbolicName"), child.attr("value"), child.attr("description"))
td.register(VariableMap)


class Security(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def JAASAuthData(self, data):
		return self.resolve(data, _type="JAASAuthData")
td.register(Security)


class J2EEResourceProperty(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def setValue(self, value):
		self.setAttr("value", value)

	def getName(self):
		return self.attr("name")	
td.register(J2EEResourceProperty)


class J2EEResourcePropertySet(Object):
	def __init__(self, id):
		Object.__init__(self, id)
		
	def setValue(self, value):
		self.setAttr("value", value)

	def getName(self):
		return self.attr("name")

	def customProperties(self, dm):
		props = self.customPropsUpdateJ2EEResourceProperties(dm)

		
td.register(J2EEResourcePropertySet)


class JMSProvider(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)

	def MQQueue(self, data):
		return self.resolve(data, _type="MQQueue")
		
	def MQQueueConnectionFactory(self, data):
		return self.resolve(data, _type="MQQueueConnectionFactory")
		
	def GenericJMSDestination(self, data):
		return self.resolve(data, _type="GenericJMSDestination")
		
	def GenericJMSConnectionFactory(self, data):
		return self.resolve(data, _type="GenericJMSConnectionFactory")

td.register(JMSProvider)		


class J2CResourceAdapter(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)

	def SIBJMSQCF(self, data):
		data["_impl"] = "SIBJMSQCF"
		return self.resolve(data, _type="J2CConnectionFactory")

	def SIBJMSQueue(self, data):
		data["_impl"] = "SIBJMSQueue"
		return self.resolve(data, _type="J2CAdminObject")
	
	def J2CActivationSpec(self, data):
		return self.resolve(data, _type="J2CActivationSpec")
	
	def SIBJMSActivationSpec(self, data):
		data["_impl"] = "SIBJMSActivationSpec"
		return self.J2CActivationSpec(data)
		
	def WMQActivationSpec(self, data):
		data["_impl"] = "WMQActivationSpec"
		return self.J2CActivationSpec(data)
		
	def J2CConnectionFactory(self, data):
		return self.resolve(data, _type="J2CConnectionFactory")

td.register(J2CResourceAdapter)


class J2CActivationSpec(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)
td.register(J2CActivationSpec)


class J2CAdminObject(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)
td.register(J2CAdminObject)


class J2CConnectionFactory(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)
td.register(J2CConnectionFactory)

class TimerManagerProvider(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)
		
	def TimerManagerInfo(self, data):
		return self.resolve(data, _type="TimerManagerInfo")
td.register(TimerManagerProvider)

class WorkManagerProvider(_Resource):
	def __init__(self, id):
		_Resource.__init__(self, id)
		
	def WorkManagerInfo(self, data):
		return self.resolve(data, _type="WorkManagerInfo")
td.register(WorkManagerProvider)

cell = getSingleton("Cell")
