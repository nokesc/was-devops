import rep_test
import sys
import time
import unittest

import java.lang.System as System

import java.io.File as File
import java.io.FileWriter as FileWriter
import java.io.PrintWriter as PrintWriter
import java.io.StringWriter as StringWriter

def debugErr(err):
	for e in err:
		print "-->" + str(type(e)) + " " + str(e)
		if e.__class__.__name__ == "org.python.core.PyTraceback":
			print e.dumpStack()

class Problem:
	def __init__(self, elementName, errorType, message, traceback):
		self.elementName = elementName
		self.errorType = errorType
		self.message = message
		self.traceback = traceback
		
	def xml(self, out):
		xml = '\t\t<%s message="%s">%s' % (self.elementName, self.message, self.traceback)
		out.println(xml)
		out.println("\t\t</%s>" % self.elementName)

class TestCase:
	def __init__(self, testMetaData, elapsed):
		self.testMetaData = testMetaData
		self.elapsed = elapsed
		self.failure = None
		self.error = None
		
	def addFailure(self, args):
		failure = Problem("failure", str(args[0]), str(args[1]), args[2].dumpStack())
		self.failure = failure
		return failure
	
	def addError(self, args):
		error = Problem("error", str(args[0]), str(args[1]), args[2].dumpStack())
		self.error = error
		return error
		
	def xml(self, out):
		xml = '\t<testcase classname="%s" name="%s" time="%s">' % (self.testMetaData.getClassname(), self.testMetaData.getMethodName(), self.elapsed)
		out.println(xml)
		if self.failure:
			self.failure.xml(out)
		if self.error:
			self.error.xml(out)
		out.println("\t</testcase>")

class TestSuite:
	def __init__(self, name):
		self.name = name
		self.testCases = []

	def xml(self, testResult, out):
		xml = '<testsuite name="%s" tests="%s" errors="%s" failures="%s"  time="%s">' % (self.name, testResult.testsRun, len(testResult.errors), len(testResult.failures), testResult.elapsed)
		out.println(xml)
		for testCase in self.testCases:
			testCase.xml(out)
		out.println("</testsuite>");
	
	def addSuccess(self, testMetaData, elapsed):
		testCase = TestCase(testMetaData, elapsed)
		self.testCases.append(testCase)
		return testCase
		
	def addFailure(self, testMetaData, err, elapsed):
		testCase = TestCase(testMetaData, elapsed)
		self.testCases.append(testCase)
		testCase.addFailure(err)
		return testCase
	
	def addError(self, testMetaData, err, elapsed):
		testCase = TestCase(testMetaData, elapsed)
		self.testCases.append(testCase)
		testCase.addError(err)
		return testCase

class TestMetaData:
	def __init__(self, test, descriptions = None):
		self.test = test
		self.description = str(self.initDescription(test, descriptions))
		self.module = str(test.__module__)
		self.shortClassname = str(test.__class__.__name__)
		self.methodName = str(test.__dict__['_TestCase__testMethodName'])
	
	def getTest(self):
		return self.test
		
	def getDescription(self):
		return self.description
	
	def initDescription(self, test, descriptions):
	        if descriptions:
	            return test.shortDescription() or str(test)
	        else:
	            return str(test)
	
	def getModule(self):
		return self.module
	
	def getShortClassname(self):
		return self.shortClassname
		
	
	def getClassname(self):
		return self.module + "." + self.shortClassname
		
	def getMethodName(self):
		return self.methodName
		
	def dump(self):
		print "\tdescription: " + self.getDescription()
		print "\tmodule:" + self.getModule()
		print "\tclassName: " + self.getClassname()
		print "\tmethodName: " + self.getMethodName()
		
		

class JUnitResult(unittest.TestResult):

	def __init__(self, descriptions = None):
		unittest.TestResult.__init__(self)
		self.descriptions = descriptions
		self.testSuite = TestSuite("unittest")
		self.start = None
	
	def write(self, dest):
		buf = FileWriter(dest)
		print "Writing JUnit Report to " + str(dest.getCanonicalPath())
		try:
			out = PrintWriter(buf)
			self.testSuite.xml(self, out)
			out.flush()
		finally:
			buf.close()
	
 	def startTest(self, test):
		print "startTest --->"
		self.start = time.time()
		TestMetaData(test, self.descriptions).dump()
		print "startTest run test"
		unittest.TestResult.startTest(self, test)
		print "startTest <---"

	def addSuccess(self, test):
		elapsed = float(time.time() - self.start)
		unittest.TestResult.addSuccess(self, test)
		self.testSuite.addSuccess(TestMetaData(test), elapsed)

	def addFailure(self, test, err):
		elapsed = float(time.time() - self.start)
		unittest.TestResult.addFailure(self, test, err)
		self.testSuite.addFailure(TestMetaData(test), err, elapsed)
		
				
	def addError(self, test, err):
		elapsed = float(time.time() - self.start)
		unittest.TestResult.addError(self, test, err)
		self.testSuite.addError(TestMetaData(test), err, elapsed)		

class JUnitOuputRunner:
	"""A test runner class that displays results in textual form.
	
	It prints out the names of tests as they are run, errors as they
	occur, and a summary of the results at the end of the test run.
	"""
	def __init__(self, descriptions = 1):
		self.descriptions = descriptions
	
	def _makeResult(self):
		return JUnitResult()

	def run(self, test):
		"Run the given test case or test suite."
		result = self._makeResult()
		startTime = time.time()
		test(result)
		stopTime = time.time()
		timeTaken = float(stopTime - startTime)
		result.elapsed = timeTaken
		run = result.testsRun
		print ("--->Ran %d test%s in %.3fs" %
		                        (run, run == 1 and "" or "s", timeTaken))
		if not result.wasSuccessful():
			print ("FAILED (")
			failed, errored = map(len, (result.failures, result.errors))
			if failed:
				print("failures=%d" % failed)
			if errored:
				print("errors=%d" % errored)
			print(")")
		else:
			print("OK")
		
		return result

def runTestsForModule(mod, dest = None):
	modName = mod.__name__
	if not dest:
		dest = dest = File("target/junit-out/%s-unittest-report.xml" % (modName))
	dest.getParentFile().mkdirs()
	testSuite = unittest.TestSuite()
	testSuite.addTest(unittest.findTestCases(mod))
	runTestSuite(testSuite, dest)
	
	
def runTestSuite(testSuite, dest):
	result = JUnitOuputRunner().run(testSuite)
	result.write(dest)
	
class ModuleHelper:
	
	def __init__(self):
		packages = System.getProperty("wsadmin.script.libraries.packages")
		self.packages = packages.split(";")
		
	def getTestModules(self):
		result = []
		for package in self.packages:
			packageDir =  File(package)
			for f in packageDir.listFiles():
				name = f.getName()
				if name.endswith("_test.py"):
					result.append(name[:-3])
		return result
		
	def getModules(self):
		result = []
		for package in self.packages:
			packageDir =  File(package)
			for f in packageDir.listFiles():
				name = f.getName()
				if name.endswith(".py") and not name.endswith("_test.py"):
					result.append(name[:-3])
		return result

moduleHelper = ModuleHelper()

def runAllTests():
	testSuite = unittest.TestSuite()
	for testModule in moduleHelper.getTestModules():
		testName = testModule[:-5] + "_test"
		exec("import " + testName)
		mod = sys.modules[testName]
		print "Adding " + testName
		testSuite.addTest(unittest.findTestCases(mod))
	dest = File("target/junit-out/all-unittest-report.xml")
	runTestSuite(testSuite, dest)
	