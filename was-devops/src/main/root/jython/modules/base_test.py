import base
import config
import data_manager as dm
import unittest
import utils

utils.debugOn = 0

cell = base.cell
node = base.node
server = base.server
mqProvider = server.getWebSphereMQJMSProvider()
sibAdapter = server.getSIBJMSResourceAdapter()


class TestBase(unittest.TestCase):
	def setUp(self):
		AdminConfig.reset()

	def tearDown(self):
		AdminConfig.reset()

	def validateResolve(self, parent, data):
		if parent:
			print "validateResolve: " + str(parent.getId())
		else:
			print "validationCreation: " + data.get("_type")
		dm.DataManager(data).show()
		
		obj = parent.asChild(data)
		self.assertEquals(None, obj)

		resolved = parent.resolve(data)
		self.assertNotEquals(None, resolved)
		
		obj = parent.asChild(data)
		self.assertNotEquals(None, obj)
		
		self.assertEquals(resolved.getId(), obj.getId())
		return obj
		
