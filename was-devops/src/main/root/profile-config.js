if (!project.getProperty("profile.init.complete")) {
	try {
		// Nashorn (Java 8) uses a new mechanism for import. Make script
		// backwards compatible.
		load("nashorn:mozilla_compat.js");
	} catch (err) {

	}
	importPackage(org.apache.tools.ant.taskdefs.condition)
	importClass(java.lang.System)
	importClass(java.io.File)

	var sp = function(key, value) {
		var interpolatedValue = project.replaceProperties(value);
		project.setNewProperty(key, interpolatedValue);
	}

	var sl = function(key, location) {
		var interpolatedValue = project.replaceProperties(location);
		var file = new File(interpolatedValue).getAbsolutePath();
		project.setNewProperty(key, file);
	}

	var p = function(key) {
		return project.getProperty(key);
	}

	var available = function(file, property, value) {
		if (file.exists()) {
			var interpolatedValue = project.replaceProperties(value);
			sp(property, interpolatedValue)
		}
	}

	var required = function(key) {
		if (!p(key)) {
			throw (key + " is required");
		}
	}

	required("was.home")
	required("profile.name")

	if (Os.isFamily("windows")) {
		sp("isWindows", "true");
		sp("shell.executable", "cmd");
		sp("shell.flags", "/C");
		sp("shell.ext", ".bat");
	} else if (Os.isFamily("unix")) {
		sp("isUnix", "true");
		sp("shell.executable", "bash");
		sp("shell.flags", "");
		sp("shell.ext", ".sh");
	} else {
		throw ("${os.name} is not supported.");
	}

	sl("was.profiles.loc", "${was.home}/profiles");
	sl("was.manageprofiles.loc",
			"${was.home}${file.separator}bin${file.separator}manageprofiles${shell.ext}");

	sp("profile.loc", "${was.profiles.loc}${file.separator}${profile.name}");
	available(new File(p("profile.loc")), "profile.loc.exists", true)

	sp("server.name", "server1");
	sp("server.logs.loc",
			"${profile.loc}${file.separator}logs${file.separator}${server.name}");

	sp("profile.bin.loc", "${profile.loc}${file.separator}bin");
	available(new File(p("profile.bin.loc")), "profile.bin.loc.exists", true)

	sp("profile.startServer.loc",
			"${profile.bin.loc}${file.separator}startServer${shell.ext}");
	sp("profile.stopServer.loc",
			"${profile.bin.loc}${file.separator}stopServer${shell.ext}");
	sp("profile.wsadmin.loc",
			"${profile.bin.loc}${file.separator}wsadmin${shell.ext}");

	if (p("profile.startingPort")) {
		sp("profile.startingPortArg", "-startingPort");
	} else {
		sp("profile.startingPortArg", "");
		sp("profile.startingPort", "");
	}
	if (p("profile.custom.lib")) {
		sp("wsadmin.custom.lib", ";${profile.custom.lib}");
	} else {
		sp("wsadmin.custom.lib", "");
	}
	if (p("profile.env")) {
		sp("wsadmin.env", "-Dprofile.env=${profile.env}");
	} else {
		sp("wsadmin.env", "-Dprofile.env=unknown");
	}

	sp("profile.init.complete", true);

	var propertySet = project.createDataType("propertyset");
	propertySet
			.appendRegex("(shell\\.|isWindows|isUnix|profile\\.|was\\.|server\\.)");
	project.addReference("wasdevops.props", propertySet);

	var echoProps = project.createTask("echoproperties");
	echoProps.addPropertyset(propertySet)
	echoProps.execute();
}