import base
import config
import fast_deploy

AdminConfig.reset()

server = base.server

mqProvider = server.getWebSphereMQJMSProvider()
print mqProvider.getConfigType()

mqProvider.MQQueue({
	"baseQueueName": "Q4",
	"jndiName": "jms/Q4",
	"name": "jms.Q4",
})

mqProvider.MQQueueConnectionFactory({
	"XAEnabled": "true",
	"host": "localhost",
	"jndiName": "jms/qcf3",
	"name": "jms.qcf2",
	"port": "1234",
	"queueManager": "QMGR1"
})

ah = fast_deploy.AppHelper(base.server, deployId="App10")

def rep():
	rep = {
		"_type": "ResourceEnvironmentProvider",
		"name": "${deployId}.rep",
		"_customProps": {
			"cust1": "val1",
			"cust2": "val2"
		}
	}
	repObj = ah.resolve(rep)
	
	ref = {
		"_type": "Referenceable",
		"factoryClassname": "org.nebula.ree.FactoryREE",
		"classname": "org.nebula.ree.REE"
	}
	refObj = repObj.resolve(ref)
	
	ree = {
		"_type": "ResourceEnvEntry",
		"jndiName": "${deployId}/ree2",
		"referenceable": refObj.getId(),
		"_customProps": {
			"cust1": "REE val1",
			"cust2": "REE val2"
		}
	}
	reeObj = repObj.resolve(ree)

rep()

AdminConfig.save()
print "Save complete"