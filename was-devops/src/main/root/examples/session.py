import code
import sys
import java.lang.System as System
import java.io.File as File

print "wsadmin.script.libraries.packages=" + str(java.lang.System.getProperty("wsadmin.script.libraries.packages"))

orgModules = sys.modules.keys()

print orgModules

lastTestMod = None
lastDemo = None

class ModuleHelper:
	
	def __init__(self):
		packages = System.getProperty("wsadmin.script.libraries.packages")
		self.packages = packages.split(";")
		
	def getTestModules(self):
		result = []
		for package in self.packages:
			packageDir =  File(package)
			for f in packageDir.listFiles():
				name = f.getName()
				if name.endswith("_test.py"):
					result.append(name[:-3])
		return result
		
	def getModules(self):
		result = []
		for package in self.packages:
			packageDir =  File(package)
			for f in packageDir.listFiles():
				name = f.getName()
				if name.endswith(".py") and not name.endswith("_test.py"):
					result.append(name[:-3])
		return result

moduleHelper = ModuleHelper()


def retainOrgModules():
	print "retainOrgModules"
	for key in sys.modules.keys():
		if key not in orgModules:
			#print "\tDeleting module " + key
			del(sys.modules[key])		

def q():
	java.lang.Runtime.getRuntime().exit(0)

def exit():
	q()

def quit():
	q()

def main():
	doExecfile("main.py")

def m():
	main()
	
def test():
	doExecfile("test.py")

def t():
	test()
		
def utAll():
	retainOrgModules()
	import unittest
	testSuite = unittest.TestSuite()
	for testModule in moduleHelper.getTestModules():
		testName = testModule[:-5] + "_test"
		exec("import " + testName)
		mod = sys.modules[testName]
		print "Adding " + testName
		testSuite.addTest(unittest.findTestCases(mod))
	unittest.TextTestRunner(verbosity=2).run(testSuite)
	
def ut(name = None):
	global lastTestMod
	if name:
		lastTestMod = name + "_test"
	retainOrgModules()
	if not lastTestMod:
		print "WARN: ut(...) requires a name if a previous test was not run"
		return
	import unittest
	exec("import " + lastTestMod)
	mod = sys.modules[lastTestMod]
	unittest.main(mod, None, [''], unittest.TextTestRunner(verbosity=2))

def demo(name = None):
	global lastDemo
	if name:
		lastDemo = "demo/" + name + ".py"
	ex(lastDemo)

def ex(name):
	doExecfile(name)

def doExecfile(name):
	retainOrgModules()
	vars = {}
	execfile(name, vars)

vars = globals().copy()
vars.update(locals())
console = code.InteractiveConsole(vars)
console.interact("wsadmin (jython %s) console" % (sys.version))
