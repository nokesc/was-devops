import base
import utils
import config

server = base.server

tibcoProvider = server.JMSProvider({
	"classpath": utils.jarPaths("tibjms/7.0.1"),
	"externalInitialContextFactory": "com.tibco.tibjms.naming.TibjmsInitialContextFactory",
	"externalProviderURL": "tibjmsnaming://tibcoServer:7222",
	"name": "Tibco"
})

config.resolveAll(tibcoProvider.GenericJMSConnectionFactory, [
	{
		"jndiName": "jms/MyAppQCF",
		"type": "QUEUE",
		"externalJNDIName": "MyAppQCF",
		"XAEnabled": "true",
		"authMechanismPreference": "BASIC_PASSWORD",
		"connectionPool": { 
			"minConnections": "1",
		},
		"mapping": {
			"mappingConfigAlias": "DefaultPrincipalMapping",
			"authDataAlias": ""
		},
		"sessionPool": {
			"minConnections": "1",
		}
	}
])

config.resolveAll(tibcoProvider.GenericJMSDestination, [
	{
		"jndiName": "jms/myApp/Q1",
		"externalJNDIName": "myApp.Q1",
		"type": "QUEUE"
	}
])

AdminConfig.save()