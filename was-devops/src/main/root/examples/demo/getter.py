class Obj:
	def __init__(self):
		self.a = "a val"
		
	def __setattr__(self, k, v):
		print "k=%s v=%s" % (k, v)
		self.__dict__[k] = v
	
	def __getitem__(self, k):
		print "getitem=" + str(k)
		
	def __getattr__(self, k):
		print "getattr=" + str(k)

o = Obj()
o.b = "b val"
o.b = "b val2"
print "o = %s" % o.b
#print "o.a = %s" % o.a
#print "o.b = %s" % o.b