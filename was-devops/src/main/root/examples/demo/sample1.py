import base
import config
import data_manager as dm

AdminConfig.reset()

abc_ds = dm.DataManager({
	"_type": "DataSource",
	"name": "jdbc.abc",
	"jndiName": "jdbc/abc",
	"host": "abc.com",
	"port": "1245"
})

abc_ds.show()

JAASAuthData = base.security.JAASAuthData
jvm = base.jvm
server = base.server

jaasAuthData = JAASAuthData({"alias": "abc", "userId": "def", "password": "xyz"})
jaasAuthData.simpleAttrs().show()
jaasAuthData = JAASAuthData({"alias": "al1", "userId": "u1", "password": "p1"})
jaasAuthData.simpleAttrs().show()


jvm.systemProperties({
	"spring.active.profiles": "WebSphere,default",
	"sysProp1": "val2"
})
jvm.show()

server.ListenerPort({
	"connectionFactoryJNDIName": "jms/qcf",
	"description": "my listener port",
	"destinationJNDIName": "jms/q1",
	"maxMessages": "1",
	"maxRetries": "2",
	"maxSessions": "10",
	"name": "LP1"
})

mtp = server.getMessageListenerService().getThreadPool()
mtp.modify({"maximumSize": "49"})
mtp.show()
print mtp.attr("minimumSize")

AdminConfig.save()