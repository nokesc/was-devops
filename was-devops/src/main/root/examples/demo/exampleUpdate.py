import config2
import config_meta

config_meta.getType("DataSource").show()

def update():
	ds1 = config2.listObjects("DataSource")[0]
	attrs = ds1.simpleAttrs()
	attrs.data['description']="my update"
	print "Modify: " + str(attrs)
	attrs.modify()

def show():
	ds1 = config2.listObjects("DataSource")[0]
	ds1.show()

show()