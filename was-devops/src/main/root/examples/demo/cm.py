import config_meta as cm

wasTypes = [ "Classloader", "ConnectionPool", "DataSource", "GenericJMSConnectionFactory", "GenericJMSDestination",\
"J2CActivationSpec", "J2CAdminObject", "J2CConnectionFactory", "J2CResourceAdapter", "JDBCProvider", "JMSProvider",\
"Library", "LibraryRef", "ListenerPort",\
"MessageListenerService", "MQConnectionFactory", "MQQueue", "MQQueueConnectionFactory",\
"Referenceable", "ResourceEnvironmentProvider", "ResourceEnvEntry",\
"SIBus", "SIBusMember", "SIBQueueLocalizationPoint",\
"StateManageable",\
"URL", "URLProvider"]

for t in wasTypes:
	cm.showWithTemplate(t)

