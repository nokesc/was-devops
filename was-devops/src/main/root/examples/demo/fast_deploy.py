import base
import fast_deploy

AdminConfig.reset()

ah = fast_deploy.AppHelper(base.server, deployId="app1")

ah.TimerManagerInfo({"jndiName": "${deployId}/tm1"})
ah.WorkManagerInfo({"jndiName": "${deployId}/wm1"})
ah.URL({"jndiName": "${deployId}/url/u2", "spec": "https://www.google.com"})

def mdbs():
	ah.MDBHelper("mdb1").createSolution()
	ah.MDBHelper("mdb2").createSolution()
	
	ah = fast_deploy.AppHelper(base.server, deployId="app2", MDBHelper=fast_deploy.WMQMDBHelper)
	ah.MDBHelper("mdb1").createSolution()
	ah.MDBHelper("mdb2").createSolution()

AdminConfig.save()