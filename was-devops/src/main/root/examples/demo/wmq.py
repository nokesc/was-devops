import base

qcf1 = {
	"_type": "MQQueueConnectionFactory",
	"name": "demo.jms.qcf2",
	"jndiName": "demo/jms/qcf2",
	"host": "localhost",
	"port": "1234",
	"queueManager": "QMGR1",
	"XAEnabled": "true"	
}

q1 = {
	"_type": "MQQueue",
	"name": "demo.jms.testq1",
	"jndiName": "demo.jms/testq1",
	"baseQueueName": "demo.testq1",
	"description": "Description for testq1"
}

as1 = {
	"_type": "J2CActivationSpec",
	"_impl": "WMQActivationSpec",
	"name": "demo.jms.wsmqAS1",
	"jndiName": "demo/jms/wmqAS1",
	"destinationJndiName": q1["jndiName"],
	"hostName": "localhost",
	"port": "1234",
	"queueManager": "QMGR1",
	"WAS_EndpointInitialState": "INACTIVE"
}

server = base.server
mqProvider = server.getWebSphereMQJMSProvider()
mqRA = server.getWebSphereMQResourceAdapter()

AdminConfig.reset()
mqProvider.MQQueueConnectionFactory(qcf1)
mqProvider.MQQueue(q1)
mqRA.WMQActivationSpec(as1)
AdminConfig.save()
print "AdminConfig.save() complete"