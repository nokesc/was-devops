import base
import config

AdminConfig.reset()

server = base.server
urlProvider = server.getDefaultURLProvider()
print str(urlProvider.getId())

urlProvider.URL({
	"jndiName": "demo1/url/google1",
	"spec": "https://www.google.com"
})

urlProvider.resolve({
	"jndiName": "demo1/url/google2",
	"spec": "https://www.google.com"
}, "URL")

AdminConfig.save()