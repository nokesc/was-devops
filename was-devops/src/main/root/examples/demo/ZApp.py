import base
import fast_deploy

AdminConfig.reset()

ah = fast_deploy.AppHelper(base.server, deployId="ZApp_1")
ah.getSIBusQCF()
ah.MDBHelper("MDB1").createSolution()

ah.URL({"jndiName": "${deployId}/url/google", "spec": "https://www.google.com"})

AdminConfig.save()