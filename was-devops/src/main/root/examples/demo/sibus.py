import base
import config

server = base.server
node = base.node
sibAdapter = server.getSIBJMSResourceAdapter()

sibusName = "sibdemo"
sibus = {
	"_type": "SIBus",
	"name": sibusName
}

sibusMember = {
	"_type": "SIBusMember",
	"_init": {
		"bus": sibusName
	},
	"server": server.getName(),
	"node": node.getName()
}

sibQueue = {
	"_type": "SIBQueue",
	"_init": {
		"bus": sibusName,
		"server": server.getName(),
		"node": node.getName()
	},
	"identifier": "demo.q1"
}

sibJMSQueue = {
	"_type": "J2CAdminObject",
	 "_impl": "SIBJMSQueue",
	 "_init": {
	 	"busName": sibusName
	 },
	"name": "demo.jms.sibQ1",
	"jndiName": "demo/jms/sibQ1",
	"QueueName": sibQueue["identifier"]
}

sibActivationSpec = {
	"_type": "J2CActivationSpec",
	 "_impl": "SIBJMSActivationSpec",
	 "_init": {
	 	"busName": sibusName
	 },
	"name": "demo.jms.sibA1",
	"jndiName": "demo/jms/sibA1",
	"destinationJndiName": sibJMSQueue["jndiName"]
}

AdminConfig.reset()

sibusObj = config.resolveSIBus(sibus)
sibusMemberObj = sibusObj.resolve(sibusMember)
sibusObj.SIBQueue(sibQueue)	
sibAdapter.SIBJMSQueue(sibJMSQueue)
sibAdapter.SIBJMSActivationSpec(sibActivationSpec)

AdminConfig.save()