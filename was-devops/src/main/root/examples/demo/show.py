import base

base.cell.simpleAttrs().show()
base.security.simpleAttrs().show()
base.node.simpleAttrs().show()
base.server.simpleAttrs().show()
base.jvm.simpleAttrs().show()
base.traceService.simpleAttrs().show()