AdminConfig.reset()

import base
cell = base.cell
node = base.node
server = base.server
security = base.security

cell.removeChildren("SIBus")
cell.removeChildren("SIBQueue")
cell.removeChildren("SIBusMember")
server.removeChildren("SIBQueueLocalizationPoint")

security.removeChildren("JAASAuthData")

server.removeChildren("ListenerPort")
server.findAndRemove("JDBCProvider", "name", "Oracle JDBC Driver (XA)")
server.findAndRemove("JMSProvider", "name", "TIBCO")
server.removeChildren("MQQueue")
server.removeChildren("MQTopic")
server.removeChildren("MQQueueConnectionFactory")
server.removeChildren("MQTopicConnectionFactory")
server.removeChildren("MQConnectionFactory")
server.removeChildren("J2CAdminObject")
server.removeChildren("J2CConnectionFactory")
server.removeChildren("J2CActivationSpec")
server.removeChildren("URL")
server.removeChildren("ResourceEnvironmentProvider")

AdminConfig.save()
