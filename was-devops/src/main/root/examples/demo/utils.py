import utils

x = {
	"DataSource": {
		"name": "fred",
		"jndiName": "jdbc/freda",
		"aged_Timeout": 20, 
		"connectionPool": {
			"maxConnections": 30,
			"minConnections": 10
		}
	}
}

print x["DataSource"]["jndiName"]
print utils.forAdminConfig(x["DataSource"])