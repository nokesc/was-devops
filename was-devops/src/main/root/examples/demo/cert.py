AdminConfig.reset()

import base
import utils

security = base.security
node = base.node
cell = base.cell

def showSec():
	
	for ks in security.children('KeyStore'):
		ks.show()
		
	for ks in security.children('Certificate'):
		ks.show()

def addCert():
	keyStoreName = "NodeDefaultTrustStore"
	keyStoreScope = "(cell):%s:(node):%s" % (cell.getName(), node.getName())
	certificateFilePath = utils.serverLibPath("certs\Dev_Cert.cer")
	base64Encoded = "false"
	certificateAlias = "Dev"
	#Binary Der Data
	AdminTask.addSignerCertificate(['-keyStoreName',  keyStoreName, '-keyStoreScope', keyStoreScope, '-certificateFilePath', certificateFilePath, '-base64Encoded', base64Encoded, '-certificateAlias', certificateAlias])
	
	
	#AdminTask.addSignerCertificate('[-keyStoreName NodeDefaultTrustStore -keyStoreScope (cell):LUSMIN-C0DPSUHNode02Cell:(node):LUSMIN-C0DPSUHNode02 -certificateFilePath sdf -base64Encoded false -certificateAlias asv ]')
	AdminConfig.save()



print AdminTask.help("addSignerCertificate")
addCert()

