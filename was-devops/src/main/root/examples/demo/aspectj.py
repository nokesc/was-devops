import base
import fast_deploy
import utils

AdminConfig.reset()

aspectjPath = utils.jarPaths("aspectjweaver/1.8.7")
print aspectjPath

jvm = base.server.getJVM()
jvm.show()
jvm.setAttr("genericJvmArguments", "-Xquickstart -javaagent:%s" % (aspectjPath))

AdminConfig.save()