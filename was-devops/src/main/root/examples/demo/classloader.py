import base
import utils
import config
import java.lang.String as String
import java.io.File as File
import java.lang.System as System

AdminConfig.reset()
server = base.server

print "Server Classloaders ->"
for child in server.children("Classloader"):
	child.show()
print "Server Classloaders <-\n"

libraryName = "mylib1"

library = server.Library({
	"name": libraryName,
	"classPath": "abc.jar;def.jar",
})
library.show()

as = server.getApplicationServer()
classloader = as.Classloader({})
classloader.show()

classloader.LibraryRef({
	"libraryName": libraryName
})

libraryName = "mylib2"

library = server.Library({
	"name": libraryName,
	"classPath": "ghi.jar;jkl.jar",
})
library.show()

as = server.getApplicationServer()
classloader = as.Classloader({"mode": "PARENT_LAST"})
classloader.show()

classloader.LibraryRef({
	"libraryName": libraryName
})

AdminConfig.save()

for child in server.children("LibraryRef"):
	child.show()