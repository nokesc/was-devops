package org.nebula.was_devops;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;
import org.nebula.was_devops.ServerLibHelper;

public class ServerLibHelperTest {

	ServerLibHelper libHelper = new ServerLibHelper(new File("src/test/resources/.was-devops"));
	
	@Test
	public void getServerLibDir() {
		ServerLibHelper libHelper = new ServerLibHelper(new File("src/test/resources/.was-devops"));
		assertTrue(libHelper.getServerLibDir().exists());
	}
	
	@Test
	public void jarPaths() {
		String aspectjPaths = libHelper.jarPaths("aspectjweaver/1.8.7");
		assertTrue(aspectjPaths.length() > 0);
		assertTrue(aspectjPaths.endsWith("aspectjweaver-1.8.7.jar"));
		assertTrue(!aspectjPaths.contains(";"));
	}
}