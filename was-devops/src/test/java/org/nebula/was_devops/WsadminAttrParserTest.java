package org.nebula.was_devops;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

public class WsadminAttrParserTest {
	public static String DATA = "[[alias mycertalias] [version 3] [size 256] [serialNumber 7309156889659848637] [issuedTo [CN=*.google.com, O=Google Inc, L=Mountain View, ST=California, C=US]] [issuedBy [CN=Google Internet Authority G2, O=Google Inc, C=US]] [fingerPrint 8D:3B:13:19:F9:31:5F:DB:2C:92:93:6B:8C:E0:90:23:11:6A:19:C6] [signatureAlgorithm SHA256withRSA(1.2.840.113549.1.1.11)] [validity [Valid from Aug 10, 2016 to Nov 2, 2016.]] ]";

	WsadminAttrParser parser = new WsadminAttrParser();

	@Test(expected = UnbalancedException.class)
	public void parse_error() {
		parser.parse("]");
	}

	@Test(expected = UnbalancedException.class)
	public void parse_error2() {
		parser.parse("[[sdf []]]]]]");
	}

	@Test()
	public void parse_nested() {
		parser.parse("[[a [b c]]]");
	}

	@Test
	public void parse() {
		Map<String, String> parse = parser.parse(DATA);
		assertEquals(9, parse.size());
		assertEquals("3", parse.get("version"));
		assertEquals("256", parse.get("size"));
		assertEquals("mycertalias", parse.get("alias"));
		assertEquals("CN=*.google.com, O=Google Inc, L=Mountain View, ST=California, C=US", parse.get("issuedTo"));

	}
}