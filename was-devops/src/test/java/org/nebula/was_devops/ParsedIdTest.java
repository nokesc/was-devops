package org.nebula.was_devops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.nebula.was_devops.ParsedId;

public class ParsedIdTest {
	public static final String ID1 = "t3(cells/Chris-THINKNode01Cell/buses/t3|sib-bus.xml#SIBus_1446947100004)";

	ParsedId helper = new ParsedId(ID1);

	@Test
	public void getName() {
		assertEquals("t3", helper.getName());
	}

	@Test
	public void getPath() {
		assertEquals("cells/Chris-THINKNode01Cell/buses/t3", helper.getPath());
	}

	@Test
	public void getFile() {
		assertEquals("sib-bus.xml", helper.getFile());
	}

	@Test
	public void getType() {
		assertEquals("SIBus", helper.getType());
	}

	@Test
	public void getXMI() {
		assertEquals("1446947100004", helper.getXMI());
	}

	@Test
	public void getCell() {
		assertEquals("Chris-THINKNode01Cell", helper.getCell());
	}
	
	@Test
	public void getBus() {
		assertEquals("t3", helper.getBus());
	}

	@Test
	public void getNode() {
		assertNull(helper.getNode());
	}

	@Test
	public void getServer() {
		assertNull(helper.getServer());
	}
}
