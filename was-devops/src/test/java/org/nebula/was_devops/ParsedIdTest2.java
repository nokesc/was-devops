
package org.nebula.was_devops;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.nebula.was_devops.ParsedId;

public class ParsedIdTest2 {
	public static final String ID1 = "SIB JMS Resource Adapter(cells/Chris-THINKNode01Cell/nodes/Chris-THINKNode01/servers/server1|resources.xml#J2CResourceAdapter_1402160561666)";

	ParsedId helper = new ParsedId(ID1);

	@Test
	public void getName() {
		assertEquals("SIB JMS Resource Adapter", helper.getName());
	}

	@Test
	public void getPath() {
		assertEquals("cells/Chris-THINKNode01Cell/nodes/Chris-THINKNode01/servers/server1", helper.getPath());
	}

	@Test
	public void getFile() {
		assertEquals("resources.xml", helper.getFile());
	}

	@Test
	public void getType() {
		assertEquals("J2CResourceAdapter", helper.getType());
	}

	@Test
	public void getXMI() {
		assertEquals("1402160561666", helper.getXMI());
	}

	@Test
	public void getCell() {
		assertEquals("Chris-THINKNode01Cell", helper.getCell());
	}

	@Test
	public void getNode() {
		assertEquals("Chris-THINKNode01", helper.getNode());
	}

	@Test
	public void getServer() {
		assertEquals("server1", helper.getServer());
	}
}