package org.nebula.was_devops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.nebula.was_devops.ParsedId;

public class ParsedIdTest3 {
	static final String ID1 = "\"Default URL Provider(cells/Chris-THINKNode01Cell/nodes/Chris-THINKNode01/servers/server1|resources.xml#URLProvider_1)\"";
	static final String ID2 = "(cells/Chris-THINKNode01Cell|security.xml#CustomUserRegistry_1)";
	static final String ID3 = "readAhead(cells/Chris-THINKNode01Cell|resources.xml#J2EEResourceProperty_1402160561938)";

	@Test
	public void idWithQuote() {
		ParsedId helper = new ParsedId(ID1);
		assertEquals("Chris-THINKNode01Cell", helper.getCell());
		assertEquals("URLProvider", helper.getType());
		assertEquals("Default URL Provider", helper.getName());
		assertEquals("/Cell:Chris-THINKNode01Cell/Node:Chris-THINKNode01/Server:server1/",
				helper.getScopeContainment());
	}

	@Test
	public void idNoName() {
		ParsedId helper = new ParsedId(ID2);
		assertEquals("Chris-THINKNode01Cell", helper.getCell());
		assertEquals("CustomUserRegistry", helper.getType());
		assertNull(helper.getName());
		assertEquals("/Cell:Chris-THINKNode01Cell/", helper.getScopeContainment());
	}

	@Test
	public void idStandard() {
		ParsedId helper = new ParsedId(ID3);
		assertEquals("Chris-THINKNode01Cell", helper.getCell());
		assertEquals("J2EEResourceProperty", helper.getType());
		assertEquals("readAhead", helper.getName());
		assertEquals("/Cell:Chris-THINKNode01Cell/", helper.getScopeContainment());
	}
}
