AdminConfig.reset()
print "main.py: AdminConfig.reset()"

import base

server = base.server
jdbcProvider = server.OracleXAJDBCProvider({})
jdbcProvider.Oracle11gDS({
	"jndiName": "jdbc/ods1",
	"URL": "myurl",
	"_customProps": {
		"connectionProperties": "oracle.jdbc.createDescriptorUseCurrentSchemaForSchemaName=true"
	}
})

AdminConfig.save()
print "main.py: AdminConfig.save()"