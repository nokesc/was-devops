package org.nebula.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Monitors <b>System.in</b> on a daemon thread and dispatches to a singleton
 * {@link SystemInListener}. The application is responsible for registering the
 * listener.
 */
public class SystemInMonitor {
	private static Logger LOGGER = Logger.getLogger(SystemInMonitor.class.getName());

	public static final SystemInMonitor INSTANCE = new SystemInMonitor();

	private volatile SystemInListener listener = null;
	private BufferedReader reader;

	private final Runnable monitor = new Runnable() {
		public void run() {
			try {
				String line;
				while ((line = reader.readLine()) != null) {
					if (listener != null) {
						listener.lineRead(line);
					}
				}
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE,
						"SystemInMonitor thread encountered an error while reading System.in, System.in is no longer being monitored",
						e);
			} finally {
				LOGGER.info("Exit SystemInMonitor");
			}
		}
	};

	private SystemInMonitor() {
		reader = new BufferedReader(new InputStreamReader(System.in, Charset.defaultCharset()));
		Thread userInputThread = new Thread(monitor, "SystemInMonitor-daemon");
		userInputThread.setDaemon(true);
		userInputThread.start();
	}

	public void setListener(SystemInListener listener) {
		synchronized (this) {
			if (this.listener != null) {
				throw new IllegalStateException("A Listener on System.in already exists");
			}
			this.listener = listener;
		}
	}

	public void clearListener(SystemInListener listener) {
		synchronized (this) {
			if (this.listener == listener) {
				this.listener = null;
			}
		}
	}
}