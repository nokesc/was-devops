package org.nebula.process;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.nebula.cli.SystemInListener;
import org.nebula.cli.SystemInMonitor;

public abstract class ProcessHandler {
	private static AtomicInteger COUNT = new AtomicInteger(0);

	public static final Process start(ProcessBuilder processBuilder) {
		try {
			return processBuilder.start();
		} catch (IOException e) {
			throw new IllegalArgumentException(processBuilder.command().toString(), e);
		}
	}

	public static File getCanonicalFile(File f) {
		try {
			return f.getCanonicalFile();
		} catch (IOException e) {
			throw new IllegalArgumentException(f.getPath(), e);
		}
	}

	public static void close(Closeable closeable) {
		try {
			closeable.close();
		} catch (IOException e) {
		}
	}

	private final Logger logger;

	public ProcessHandler() {
		super();
		logger = Logger.getLogger(getClass().getName());
	}

	public int run() {
		ProcessBuilder processBuilder = createProcessBuilder();
		Process process = start(processBuilder);
		Integer result = null;
		try {
			handle(process);
			try {
				result = process.waitFor();
				return result;
			} catch (InterruptedException e) {
				logger.log(Level.WARNING, "process interrupted: " + processBuilder.command().toString());
			}
		} finally {
			if (result == null) {
				process.destroy();
				result = process.exitValue();
			}
		}
		return result;
	}

	protected abstract ProcessBuilder createProcessBuilder();

	protected abstract void handle(Process process);

	protected void sendProcessOutputToSysout(Process process) {
		final InputStream processIS = process.getInputStream();
		Thread outputWriterThread = new Thread(new Runnable() {
			public void run() {
				InputStreamReader reader = new InputStreamReader(processIS);
				try {
					char[] chars = new char[4096 * 4];
					int read;
					while ((read = reader.read(chars)) > 0) {
						System.out.append(new String(chars, 0, read));
					}
				} catch (Exception e) {
					e.printStackTrace(System.out);
				} finally {
					logger.log(Level.FINEST, "Process is closed");
				}
			}
		}, "ProcToSysOut-" + COUNT.getAndIncrement());
		outputWriterThread.start();
	}

	protected void sendUserInputToProcess(Process process) {
		final PrintWriter processOut = new PrintWriter(new OutputStreamWriter(process.getOutputStream()));
		SystemInMonitor.INSTANCE.setListener(new SystemInListener() {
			public void lineRead(String line) {
				processOut.println(line);
				processOut.flush();
			}
		});
	}

}