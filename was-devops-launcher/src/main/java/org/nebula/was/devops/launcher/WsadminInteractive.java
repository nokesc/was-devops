package org.nebula.was.devops.launcher;

public class WsadminInteractive extends AbstractWsadmin {

	public WsadminInteractive() {
		super();
	}

	@Override
	protected void handle(Process process) {
		sendProcessOutputToSysout(process);
		sendUserInputToProcess(process);
	}
}