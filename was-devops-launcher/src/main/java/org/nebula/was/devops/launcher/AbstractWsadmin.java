package org.nebula.was.devops.launcher;

import java.io.File;

import org.nebula.process.ProcessHandler;

/**
 * Sets up was_devops jython modules and classpath and then calls wsadmin.
 */
public abstract class AbstractWsadmin extends ProcessHandler {

	/**
	 * The location where the project specific jython files (e.g. main.py) are
	 * stored. This is also used as the working directory for the wsadmin
	 * process.
	 */
	private File projectJythonDir;

	/**
	 * The <b>wsadmin.sh</b> or <b>wsadmin.bat</b> file located in the
	 * <i>was-profile</i>/bin directory.
	 */
	private File wsadminShell;

	/**
	 * The location of the was-devops library files to use. Default location is
	 * <b><i>user.home</i>/.was-devops/<i>version</i></b>
	 */
	private File wasDevopsDir;

	/**
	 * The jython file to run for wsadmin. The default is <b>session.py</b>
	 */
	private String file = "session.py";

	/**
	 * Passed to wsadmin as the -connType parameter. Default is NONE
	 */
	private String connType = "NONE";

	public AbstractWsadmin() {
		super();
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getConnType() {
		return connType;
	}

	public void setConnType(String connType) {
		this.connType = connType;
	}

	public File getProjectJythonDir() {
		return projectJythonDir;
	}

	public void setProjectJythonDir(File jythonDir) {
		this.projectJythonDir = getCanonicalFile(jythonDir);
	}

	public File getWasDevopsDir() {
		return wasDevopsDir;
	}

	public void setWasDevopsDir(File wasDevopsDir) {
		this.wasDevopsDir = getCanonicalFile(wasDevopsDir);
	}

	public File getWsadminShell() {
		return wsadminShell;
	}

	public void setWsadminShell(File wsadminFile) {
		this.wsadminShell = getCanonicalFile(wsadminFile);
	}

	protected ProcessBuilder createProcessBuilder() {
		String classesPath = new File(wasDevopsDir, "classes").getPath();
		System.out.println("classesPath=" + classesPath);
		
		String wsadminSciptLibPackages = new File(wasDevopsDir, "jython/modules").getPath();
		System.out.println("wsadminSciptLibPackages=" + wsadminSciptLibPackages);

		System.out.println("wsadminShell=" + wsadminShell.getPath());
		System.out.println("connType=" + connType);
		System.out.println("projectJythonDir=" + projectJythonDir);
		System.out.println("file=" + file);
		ProcessBuilder processBuilder = new ProcessBuilder(wsadminShell.getPath(), "-wsadmin_classpath", classesPath,
				"-javaoption", String.format("\"-Dwsadmin.script.libraries.packages=%s\"", wsadminSciptLibPackages),
				"-connType", connType, "-f", file);
		processBuilder.directory(projectJythonDir);
		processBuilder.redirectErrorStream(true);
		return processBuilder;
	}
}