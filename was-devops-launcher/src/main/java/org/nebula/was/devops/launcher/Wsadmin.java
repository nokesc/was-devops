package org.nebula.was.devops.launcher;

public class Wsadmin extends AbstractWsadmin {

	public Wsadmin() {
		super();
	}

	@Override
	protected void handle(Process process) {
		sendProcessOutputToSysout(process);
	}
}