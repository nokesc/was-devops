package org.nebula.was_devops.launcher;

import java.io.File;

import org.nebula.was.devops.launcher.WsadminInteractive;

public class AppSrv01InteractiveLauncher {
	public static void main(String[] args) {
		File wsadminShell = AppSrv01.WSADMIN_SHELL;
		File projectJythonDir = WAS_DEVOPS.EXAMPLES_PROJECT_DIR;
		File wasDevopsDir = WAS_DEVOPS.SRC_DIR;

		WsadminInteractive wsadminInteractive = new WsadminInteractive();
		wsadminInteractive.setProjectJythonDir(projectJythonDir);
		wsadminInteractive.setWsadminShell(wsadminShell);
		wsadminInteractive.setWasDevopsDir(wasDevopsDir);
		wsadminInteractive.run();
	}
}