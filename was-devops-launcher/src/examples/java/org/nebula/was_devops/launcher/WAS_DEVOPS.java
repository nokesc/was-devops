package org.nebula.was_devops.launcher;

import java.io.File;

public class WAS_DEVOPS {
	public static File SRC_DIR = new File("../was-devops/src/main/root");
	public static File EXAMPLES_PROJECT_DIR = new File(WAS_DEVOPS.SRC_DIR, "examples");
}