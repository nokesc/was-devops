package org.nebula.was_devops.launcher;

import java.io.File;

import org.nebula.was.devops.launcher.Wsadmin;

public class AppSrv01Launcher {
	public static void main(String[] args) {
		File wsadminShell = AppSrv01.WSADMIN_SHELL;
		File projectJythonDir = WAS_DEVOPS.EXAMPLES_PROJECT_DIR;
		File wasDevopsDir = WAS_DEVOPS.SRC_DIR;

		Wsadmin wsadmin = new Wsadmin();
		wsadmin.setProjectJythonDir(projectJythonDir);
		wsadmin.setWsadminShell(wsadminShell);
		wsadmin.setWasDevopsDir(wasDevopsDir);
		wsadmin.run();
	}
}
