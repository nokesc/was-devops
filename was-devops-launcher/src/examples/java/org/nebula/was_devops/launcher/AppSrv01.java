package org.nebula.was_devops.launcher;

import java.io.File;
import java.util.Arrays;

public class AppSrv01 {
	public static String WAS_HOME = getFirstEnvValue("WAS90_HOME", "WAS85_HOME", "WAS80_HOME");
	public static File WSADMIN_SHELL = new File(WAS_HOME, "profiles\\AppSrv01\\bin\\wsadmin.bat");

	public static String getFirstEnvValue(String... keys) {
		for (String key : keys) {
			String value = System.getenv(key);
			if (value != null) {
				return value;
			}
		}
		throw new IllegalArgumentException("Unable to find ENV value for any key: " + Arrays.asList(keys));
	}

	public static void main(String[] args) {
		System.out.println(WSADMIN_SHELL);
	}
}